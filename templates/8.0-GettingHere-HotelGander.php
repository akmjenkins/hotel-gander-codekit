<?php $bodyclass = 'error404'; ?>
<?php include('inc/i-header.php'); ?>

	<div class="hero single">
		<div class="swiper-wrapper">
			<div class="swipe" data-controls="true" data-auto="7">
				<div class="swipe-wrap">

					<div data-src="../assets/images/temp/hero/hero-inside-2.jpg">
						<div class="item">&nbsp;</div>
						
						<div class="caption">
							<div class="sw">
								<h1 class="title">Getting Here</h1>
								<span class="sub">Aliquam Risus Eros.</span>

							</div><!-- .sw -->
						</div><!-- .caption -->
					</div>
					
				</div><!-- .swipe-wrap -->
			</div><!-- .swipe -->
		</div><!-- .swiper-wrapper -->
	</div><!-- .hero -->			

	<div class="body">
	
		<div class="breadcrumbs">
			<div class="sw">
				<a href="#" class="fa fa-abs fa-home">Home</a>
					<a href="#">Getting Here</a>
			</div><!-- .sw -->
		</div><!-- .breadcrumbs -->
			
		<section class="sw cf">
			<div class="main-body">
				<div class="article-body">
					
						<p class="excerpt">
							Donec at augue nec ante hendrerit venenatis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Donec adipiscing ut sem tempus sodales. 
						</p>
						
						<p>
							Nullam malesuada leo in risus dictum ullamcorper. Fusce elementum, lorem vel varius aliquam, justo massa dignissim tortor, in tempor eros arcu nec ipsum. In tempus mattis libero, 
							sit amet placerat nisl ultrices in. Nulla a fermentum sem. Proin in diam ut enim tristique lobortis. Phasellus porta mollis erat, quis porttitor purus vehicula eu. Vestibulum sit amet 
							lectus magna. Nam et rhoncus turpis. Sed nec feugiat ligula. Donec at erat eros. Quisque eu convallis dui.
						</p>
					
				</div><!-- .article-body -->
			</div><!-- .main-body -->
		</section><!-- .sw -->
		
		<section class="filter-section nopad">
		
			<div class="tab-wrapper">
		
				<div class="filter-bar tab-controls direction-controls">
					<div class="sw">
					
						<div class="selector fa fa-angle-down fa-abs mobile-selector">
							<select class="tab-controller">
								<option selected>By Air</option>
								<option>By Land</option>
								<option>By Water</option>
							</select>
							<span class="value">&nbsp;</span>
						</div><!-- .selector -->
					
						<div class="tbl-wrap">
							<div class="tbl">
							
								<div class="button dark-fill tab-control col-3 center direction-button selected">
									<span><i class="fa fa-plane"></i>By Air</span>
								</div>
								
								<div class="button dark-fill tab-control col-3 center direction-button">
									<span><i class="fa fa-car"></i>By Land</span>
								</div>
								
								<div class="button dark-fill tab-control col-3 center direction-button">
									<span><i class="fa fa-life-ring"></i>By Water</span>
								</div>
								
							</div><!-- .tbl -->
						</div><!-- .tbl-wrap -->
						
					</div><!-- .sw -->
				</div><!-- .filter-bar -->

				<div class="tab-holder directions-tabs">
				
					<div class="tab selected">
					
						<div class="embedded-gmap">
							<iframe
							frameborder="0" style="border:0"
							src="https://www.google.com/maps/embed/v1/directions?key=<?php echo $GOOGLE_MAPS_API_KEY; ?>
									&origin=St. John's International Airport
									&destination=Hotel Gander, Gander NL">
							</iframe>
						</div><!-- .embedded-gmap -->

					</div><!-- .tab -->
					
					<div class="tab">
						
						<div class="sw">
							<div class="grid">
								<div class="col-2 col sm-col-1">
								
									<h3>From Address:</h3>
								
									<form action="/" class="single-form" id="street-form">
										<fieldset>
											<input type="text" placeholder="Enter your address...">
											<button type="submit" class="fa fa-abs fa-search">Search</button>
										</fieldset>
									</form>
									
								</div><!-- .col -->
								<div class="col-2 col sm-col-1 geolocation-only">
									
									<h3>From Current Location:</h3>
									<button class="button geolocate dark-fill" id="directions-geolocate">
										Get My Location
										<i class="fa fa-location-arrow"></i>
									</button>
									
								</div><!-- .col -->
							</div><!-- .grid -->
						</div><!-- .sw -->
					
						<div id="street-map" class="embedded-gmap" data-destination="Hotel Gander, Gander, NL">
						</div><!-- #street-map -->
					
					</div><!-- .tab -->
					
					<div class="tab">
					
						<div class="embedded-gmap">
							<iframe
							frameborder="0" style="border:0"
							src="https://www.google.com/maps/embed/v1/directions?key=<?php echo $GOOGLE_MAPS_API_KEY; ?>
									&origin=Marina+Atlantic, Channel Port Aux Basques
									&destination=Hotel Gander, Gander NL">
							</iframe>
						</div><!-- .embedded-gmap -->
						
					</div><!-- .tab -->
					
				</div><!-- .tab-holder -->
			
			</div><!-- .tab-wrapper -->
			
		</section><!-- .filter-section -->

	
	</div><!-- .body -->
			

<?php include('inc/i-footer.php'); ?>