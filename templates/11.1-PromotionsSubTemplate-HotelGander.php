<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

	<div class="hero single">
		<div class="swiper-wrapper">
			<div class="swipe" data-controls="true" data-auto="7">
				<div class="swipe-wrap">

					<div data-src="../assets/images/temp/hero/hero-inside-3.jpg">
						<div class="item">&nbsp;</div>
						
						<div class="caption">
							<div class="sw">
								<h1 class="title">Morbi Malesuada Nibh non Blandit Semper</h1>
								<span class="sub">Aliquam Risus Eros</span>

							</div><!-- .sw -->
						</div><!-- .caption -->
					</div>
					
				</div><!-- .swipe-wrap -->
			</div><!-- .swipe -->
		</div><!-- .swiper-wrapper -->
	</div><!-- .hero -->			

	<div class="body">
	
		<article>
	
			<div class="breadcrumbs">
				<div class="sw">
					<a href="#" class="fa fa-abs fa-home">Home</a>
						<a href="#">Promotions</a>
						<a href="#">Morbi Malesuada Nibh</a>
				</div><!-- .sw -->
			</div><!-- .breadcrumbs -->
			
			<section class="sw cf">
				<div class="main-body with-sidebar">
					<div class="article-body">
						
						
						<div class="article-head">
							<span class="price">$99.00*</span>
							<time datetime="2014-09-14" class="meta sprite-before calendar">Promotion Ends July 21, 2014</time>
							
						</div><!-- .article-head -->
						
						<p class="excerpt">
							Donec at augue nec ante hendrerit venenatis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Donec adipiscing ut sem tempus sodales. 
						</p>
						
						<p>Nullam malesuada leo in risus dictum ullamcorper. Fusce elementum, lorem vel varius aliquam, justo massa dignissim tortor, in tempor eros arcu nec ipsum. 
						In tempus mattis libero, sit amet placerat nisl ultrices in. Nulla a fermentum sem. Proin in diam ut enim tristique lobortis. Phasellus porta mollis erat, quis 
						porttitor purus vehicula eu. Vestibulum sit amet lectus magna. Nam et rhoncus turpis. Sed nec feugiat ligula. Donec at erat eros. Quisque eu convallis dui.</p>
						
					</div><!-- .article-body -->
				</div><!-- .main-body -->
				<aside class="sidebar">
					
					<div>
					
						<div class="dark-bg head">
							<span class="h5-style">Archives</span>
						</div><!-- .head -->
						
						<div class="accordion">
						
							<div class="accordion-item">
								<div class="accordion-item-handle fa fa-angle-down">
								
									<span class="count">12</span> 2014
								
								</div><!-- .accordion-item-handle -->
								<div class="accordion-item-content">
									
									<ul>
										<li><a href="#">July (3)</a></li>
										<li><a href="#">June (5)</a></li>
										<li><a href="#">May (3)</a></li>
										<li><a href="#">April (1)</a></li>
									</ul>
									
								</div><!-- .accordion-item-content -->
							</div><!-- .accordion-item -->
							
							<div class="accordion-item">
								<div class="accordion-item-handle fa fa-angle-down">
								
									<span class="count">15</span> 2013
								
								</div><!-- .accordion-item-handle -->
								<div class="accordion-item-content">
									
									<ul>
										<li><a href="#">July (3)</a></li>
										<li><a href="#">June (5)</a></li>
										<li><a href="#">May (3)</a></li>
										<li><a href="#">April (1)</a></li>
									</ul>
									
								</div><!-- .accordion-item-content -->
							</div><!-- .accordion-item -->
							
							<div class="accordion-item">
								<div class="accordion-item-handle fa fa-angle-down">
								
									<span class="count">18</span> 2012
								
								</div><!-- .accordion-item-handle -->
								<div class="accordion-item-content">
									
									<ul>
										<li><a href="#">July (3)</a></li>
										<li><a href="#">June (5)</a></li>
										<li><a href="#">May (3)</a></li>
										<li><a href="#">April (1)</a></li>
									</ul>
									
								</div><!-- .accordion-item-content -->
							</div><!-- .accordion-item -->
							
						</div><!-- .accordion -->
					</div>
					
				</aside><!-- .sidebar -->
			</section><!-- .sw -->
		
		</article>
	
	</div><!-- .body -->
			

<?php include('inc/i-footer.php'); ?>