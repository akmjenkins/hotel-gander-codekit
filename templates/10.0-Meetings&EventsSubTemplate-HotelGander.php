<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

	<div class="hero single">
		<div class="swiper-wrapper">
			<div class="swipe" data-controls="true" data-auto="7">
				<div class="swipe-wrap">

					<div data-src="../assets/images/temp/hero/hero-inside-4.jpg">
						<div class="item">&nbsp;</div>
						
						<div class="caption">
							<div class="sw">
								<h1 class="title">Meetings</h1>
								<span class="sub">Aliquam Risus Eros.</span>

							</div><!-- .sw -->
						</div><!-- .caption -->
					</div>
					
				</div><!-- .swipe-wrap -->
			</div><!-- .swipe -->
		</div><!-- .swiper-wrapper -->
	</div><!-- .hero -->			

	<div class="body">
	
		<div class="breadcrumbs">
			<div class="sw">
				<a href="#" class="fa fa-abs fa-home">Home</a>
					<a href="#">Meetings &amp; Events</a>
					<a href="#">Meetings</a>
			</div><!-- .sw -->
		</div><!-- .breadcrumbs -->
	
		<article>
			
			<section class="sw cf">
				<div class="main-body with-sidebar">
					<div class="article-body">
						
						<p class="excerpt">
							Donec at augue nec ante hendrerit venenatis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Donec adipiscing ut sem tempus sodales. 
						</p>
						
						<p>
							Nullam malesuada leo in risus dictum ullamcorper. Fusce elementum, lorem vel varius aliquam, justo massa dignissim tortor, in tempor eros arcu nec ipsum. In tempus mattis libero, 
							sit amet placerat nisl ultrices in. Nulla a fermentum sem. Proin in diam ut enim tristique lobortis. Phasellus porta mollis erat, quis porttitor purus vehicula eu. Vestibulum sit amet 
							lectus magna. Nam et rhoncus turpis. Sed nec feugiat ligula. Donec at erat eros. Quisque eu convallis dui.
						</p>
						
						<h2>Donec at Augue nec Ante</h2>
						
						<p>
							Etiam ac cursus nisl. In nec hendrerit felis. Cras id sem at ante euismod semper. Sed placerat nisi tellus. Integer enim ipsum, suscipit non aliquam a, pulvinar eget turpis. 
							Suspendisse potenti. Maecenas laoreet eget nulla in pharetra. Cras eget fermentum dui. Mauris volutpat ultrices erat sit amet aliquam. Nunc dolor mauris, mattis at 
							dignissim at, malesuada eget turpis. 
						</p>

						<p>
							Mauris dapibus egestas dui, in fringilla arcu condimentum posuere. Sed luctus diam sit amet erat iaculis sollicitudin. Etiam id varius eros. Nunc ornare scelerisque nisl, 
							quis dapibus ligula facilisis ut. Quisque vel urna sit amet nunc lacinia iaculis.
						</p>
						
						
						<div class="hgroup">
							<h2 class="title">Photo Gallery</h2>
							<span class="h5-style subtitle">Aliquam Risus Eros.</span>
						</div><!-- .hgroup -->
						
						<div class="gallery-thumbs">
							<div class="gallery-thumb">
								<a href="../assets/images/temp/gallery-thumb-1.jpg" class="mpopup" data-gallery="photo-gallery" style="background-image: url(../assets/images/temp/gallery-thumb-1.jpg);"></a>
							</div><!-- .gallery-thumb -->
							<div class="gallery-thumb">
								<a href="../assets/images/temp/gallery-thumb-2.jpg" class="mpopup" data-gallery="photo-gallery" style="background-image: url(../assets/images/temp/gallery-thumb-2.jpg);"></a>
							</div><!-- .gallery-thumb -->
							<div class="gallery-thumb">
								<a href="../assets/images/temp/gallery-thumb-3.jpg" class="mpopup" data-gallery="photo-gallery" style="background-image: url(../assets/images/temp/gallery-thumb-3.jpg);"></a>
							</div><!-- .gallery-thumb -->
							<div class="gallery-thumb">
								<a href="../assets/images/temp/gallery-thumb-4.jpg" class="mpopup" data-gallery="photo-gallery" style="background-image: url(../assets/images/temp/gallery-thumb-4.jpg);"></a>
							</div><!-- .gallery-thumb -->
						</div><!-- .gallery -->
						
					</div><!-- .article-body -->
				</div><!-- .main-body -->
				<aside class="sidebar">
					
					<div class="side-nav">
						<a href="#" class="full button dark-fill reverse selected">Link One</a>
						<a href="#" class="full button dark-fill reverse">Link Two</a>
						<a href="#" class="full button dark-fill reverse">Link Three</a>
					</div>
					
					<div class="dark-bg callout-wrap">
						<div class="callout" style="background-image: url(../assets/images/temp/callout.jpg);">
							<div class="content">
								<h3 class="title">Interest in Hosting Your Meeting With Us?</h3>
								
								<form action="/" method="post" class="body-form cf">
									<fieldset>
										<input type="email" placeholder="Email address...">
										<button type="submit" class="button">Request Info</button>
									</fieldset>
								</form>
								
								<span class="phone">Or call us at 1.888.888.8888</span>
							</div><!-- .content -->
						</div><!-- .callout -->
					</div><!-- .dark-bg -->
					
				</aside><!-- .sidebar -->
			</section><!-- .sw -->
		
		</article>
	
	</div><!-- .body -->
			

<?php include('inc/i-footer.php'); ?>