<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

	<div class="hero single">
		<div class="swiper-wrapper">
			<div class="swipe" data-controls="true" data-auto="7">
				<div class="swipe-wrap">

					<div data-src="../assets/images/temp/hero/hero-inside-2.jpg">
						<div class="item">&nbsp;</div>
						
						<div class="caption">
							<div class="sw">
								<h1 class="title">Dining</h1>
								<span class="sub">Just Like Home</span>

							</div><!-- .sw -->
						</div><!-- .caption -->
						
					</div>
					
				</div><!-- .swipe-wrap -->
			</div><!-- .swipe -->
		</div><!-- .swiper-wrapper -->
	</div><!-- .hero -->			

	<div class="body">
	
		<div class="breadcrumbs">
			<div class="sw">
				<a href="#" class="fa fa-abs fa-home">Home</a>
				<a href="#">Dining</a>
			</div><!-- .sw -->
		</div><!-- .breadcrumbs -->
	
		<section class="white">
			<article>
				<div class="sw cf">
					<div class="main-body">
						<div class="article-body">
						
							<div class="hotel-gallery">
								
								<div class="square-thumbs">
									<a href="../assets/images/temp/hotel-gallery/1.jpg" data-gallery="hotel-gallery" class="mpopup large-thumb" style="background-image: url(../assets/images/temp/hotel-gallery/1.jpg);"></a>
									<div><a href="../assets/images/temp/photos-videos/1.jpg" data-gallery="hotel-gallery" class="mpopup pad-thumb square" style="background-image: url(../assets/images/temp/photos-videos/1.jpg);"></a></div>
									<div><a href="../assets/images/temp/photos-videos/2.jpg" data-gallery="hotel-gallery" class="mpopup pad-thumb square" style="background-image: url(../assets/images/temp/photos-videos/2.jpg);"></a></div>
									<div><a href="../assets/images/temp/photos-videos/7.jpg" data-gallery="hotel-gallery" class="mpopup pad-thumb square" style="background-image: url(../assets/images/temp/photos-videos/7.jpg);"></a></div>
									<div><a href="../assets/images/temp/photos-videos/5.jpg" data-gallery="hotel-gallery" class="mpopup pad-thumb square" style="background-image: url(../assets/images/temp/photos-videos/5.jpg);"></a></div>
								</div><!-- .square-thumbs -->
								
							</div><!-- .hotel-gallery -->

							<hr />

							<img src="../assets/images/temp/hotel-gallery/sidebar-2.jpg" class="alignright">

							<div class="hgroup">
								<h2 class="title">About Dining</h2>
								<span class="subtitle h6-style">Lorem ipsum dolor sit amet, consectetur adipiscing elit</span>
							</div><!-- .hgroup -->

							<p>
								Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra 
								justo commodo. Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. 
								Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus sapien nunc eget odio.
							</p>

							<p>
								Phasellus at commodo libero, nec sollicitudin nibh. Aenean at lectus scelerisque, porta urna tincidunt, dictum lectus. Mauris faucibus ut nibh quis mollis. 
								Maecenas mattis, urna eleifend ullamcorper fermentum, nisl velit fermentum nunc, sed adipiscing dolor erat ut lorem. Nulla sagittis ipsum a ultricies accumsan.
							</p>

							<hr />

							<div class="hgroup">
								<h2 class="title">Dining Features</h2>
								<span class="subtitle h6-style">Lorem ipsum dolor sit amet, consectetur adipiscing elit</span>
							</div><!-- .hgroup -->

							<div class="feature-block">
								<div class="img-wrap">
									<div class="pad-thumb square" style="background-image: url(../assets/images/temp/hotel-gallery/feat-1.jpg);"></div>
								</div>
								
								<span class="h4-style">Feature Name</span>
								<p>Maecenas convallis interdum ullamcorper. Sed accumsan eros augue, id rutrum dolor convallis non. Mauris malesuada ultrices eleifend. Curabitur id justo 
								quis eros ornare porta a quis tellus. Donec tristique aliquam tortor id ultrices.</p>
							</div><!-- .feature-block -->

							<div class="feature-block">
								<div class="img-wrap">
									<div class="pad-thumb square" style="background-image: url(../assets/images/temp/hotel-gallery/feat-2.jpg);"></div>
								</div>
								
								<span class="h4-style">Feature Name</span>
								<p>Maecenas convallis interdum ullamcorper. Sed accumsan eros augue, id rutrum dolor convallis non. Mauris malesuada ultrices eleifend. Curabitur id justo 
								quis eros ornare porta a quis tellus. Donec tristique aliquam tortor id ultrices.</p>
							</div><!-- .feature-block -->

							<div class="feature-block">
								<div class="img-wrap">
									<div class="pad-thumb square" style="background-image: url(../assets/images/temp/hotel-gallery/feat-3.jpg);"></div>
								</div>
								
								<span class="h4-style">Feature Name</span>
								<p>Maecenas convallis interdum ullamcorper. Sed accumsan eros augue, id rutrum dolor convallis non. Mauris malesuada ultrices eleifend. Curabitur id justo 
								quis eros ornare porta a quis tellus. Donec tristique aliquam tortor id ultrices.</p>
							</div><!-- .feature-block -->

							<div class="feature-block">
								<div class="img-wrap">
									<div class="pad-thumb square" style="background-image: url(../assets/images/temp/hotel-gallery/feat-4.jpg);"></div>
								</div>
								
								<span class="h4-style">Feature Name</span>
								<p>Maecenas convallis interdum ullamcorper. Sed accumsan eros augue, id rutrum dolor convallis non. Mauris malesuada ultrices eleifend. Curabitur id justo 
								quis eros ornare porta a quis tellus. Donec tristique aliquam tortor id ultrices.</p>
							</div><!-- .feature-block -->

							<div class="feature-block">
								<div class="img-wrap">
									<div class="pad-thumb square" style="background-image: url(../assets/images/temp/hotel-gallery/feat-5.jpg);"></div>
								</div>
								
								<span class="h4-style">Feature Name</span>
								<p>Maecenas convallis interdum ullamcorper. Sed accumsan eros augue, id rutrum dolor convallis non. Mauris malesuada ultrices eleifend. Curabitur id justo 
								quis eros ornare porta a quis tellus. Donec tristique aliquam tortor id ultrices.</p>
							</div><!-- .feature-block -->
						
						</div><!-- .article-body -->
					</div><!-- .main-body -->
				</div><!-- .sw.cf -->
			</article>
		</section><!-- .white -->
		
	</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>