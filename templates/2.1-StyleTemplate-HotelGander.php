<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

	<div class="hero single">
		<div class="swiper-wrapper">
			<div class="swipe" data-controls="true" data-auto="7">
				<div class="swipe-wrap">

					<div data-src="../assets/images/temp/hero/hero-inside-2.jpg">
						<div class="item">&nbsp;</div>
						
						<div class="caption">
							<div class="sw">
								<h1 class="title">Styles</h1>
								<span class="sub">Across the Island</span>

							</div><!-- .sw -->
						</div><!-- .caption -->
					</div>
					
				</div><!-- .swipe-wrap -->
			</div><!-- .swipe -->
		</div><!-- .swiper-wrapper -->
	</div><!-- .hero -->			

	<div class="body">
	
		<div class="breadcrumbs">
			<div class="sw">
				<a href="#" class="fa fa-abs fa-home">Home</a>
				<a href="#">Styles</a>
			</div><!-- .sw -->
		</div><!-- .breadcrumbs -->
	
		<div class="article-body">
			
			<!-- TYPOGRAPHY -->
			<section>
				<div class="sw">
				
					<blockquote>
						<strong>NOTE:</strong> Styles in this section are applied to any of the following elements as long as they are within an element with a class of <strong>article-body</strong>.
					</blockquote>
				
					<h2>Typography</h2>
					
					<div class="grid collapse-800">
						<div class="col-2 col">
						
							<h1>h1 - 28px Open Sans Light</h1>
							<h2>h2 - 24px Open Sans Light</h2>
							<h3>h3 - 20px Open Sans Light</h3>
							<h4>h4 - 18px Open Sans Light</h4>
							<h5>h5 - 16px Open Sans Light</h5>
							<h6>h6 - 14px Open Sans Light</h6>
						
						</div><!-- .col -->
						<div class="col-2 col">
						
							<p>Paragraph - 13px Open Sans Light - Etiam accumsan hendrerit diam, quis ultricies sem. Sed imperdiet ipsum lorem, vitae suscipit diam tempor ut. Phasellus eget massa non metus scelerisque aliquet id id orci. Etiam a quam eget arcu bibendum aliquam a ut risus. Duis at elit sed mi pharetra imperdiet quis quis leo. Suspendisse ut mauris sit amet erat faucibus consectetur ut in mi. Nulla facilisi. In sed euismod risus, nec mollis nibh. Integer eu lorem ut lacus commodo lobortis id facilisis ligula.</p>
						
						</div><!-- .col -->
						
						<div class="col-1 col">
							<blockquote>
								Blockquote - 16px Open Sans Light - Aenean id augue congue eros accumsan laoreet vitae quis erat. Donec sollicitudin id nisi at iaculis. Phasellus porttitor, lacus at pulvinar tempus, nisi arcu ultricies quam, eu luctus massa sem posuere ipsum. Curabitur et sagittis lorem. Maecenas quis eleifend justo. Donec ultricies, dolor sit amet consequat bibendum, nulla nisl venenatis neque, imperdiet imperdiet dui augue vitae augue.
							</blockquote>
						</div>
						
					</div><!-- .grid -->

				</div><!-- .sw -->
			</section>
			
			<!-- BUTTONS -->
			<section class="light split-screen dark-right collapse-800">
				<div class="sw">
					<div class="grid split-screen-grid collapse-800">
						<div class="col-2 col">
							<div class="item">
								<div>
									<h2>Buttons &amp; Social (Light Background)</h2>
									
									<br />
									
									<h5>Buttons</h5>
									
									<button class="button">Button</button>
									<button class="button hover">Hover</button>
									<button class="button" disabled="disabled">Disabled</button>
									
									<br />
									<br />
									
									<h5>Arrow Buttons</h5>
									
									<span class="fa fa-abs fa-angle-right arrow-button">Next</span>
									<span class="fa fa-abs fa-angle-right arrow-button hovered">Next</span>
									<span class="fa fa-abs fa-angle-right arrow-button disabled">Next</span>
									
									<br />
									
									<span class="fa fa-abs fa-angle-left arrow-button">Prev</span>
									<span class="fa fa-abs fa-angle-left arrow-button hovered">Prev</span>
									<span class="fa fa-abs fa-angle-left arrow-button disabled">Prev</span>
									
									<br />
									<br />
									
									<h5>Social Icons</h5>
										<?php include('inc/i-social.php'); ?>
									<div class="hovered">
										<?php include('inc/i-social.php'); ?>
									</div><!-- .hovered -->
								</div>
							</div><!-- .item -->
						</div><!-- .col -->
						<div class="col-2 col">
							<div class="item dark-bg">
								<div>
									<h2>Buttons &amp; Social (Light Background)</h2>
									
									<br />
									
									<h5>Buttons</h5>
									
									<button class="button dark-bg">Button</button>
									<button class="button hover dark-bg">Hover</button>
									<button class="button dark-bg" disabled="disabled">Disabled</button>
									
									<br />
									<br />
									
									<h5>Arrow Buttons</h5>
									
									<span class="fa fa-abs fa-angle-right arrow-button">Next</span>
									<span class="fa fa-abs fa-angle-right arrow-button hovered">Next</span>
									<span class="fa fa-abs fa-angle-right arrow-button disabled">Next</span>
									
									<br />
									
									<span class="fa fa-abs fa-angle-left arrow-button">Prev</span>
									<span class="fa fa-abs fa-angle-left arrow-button hovered">Prev</span>
									<span class="fa fa-abs fa-angle-left arrow-button disabled">Prev</span>
									
									<br />
									<br />
									
									<h5>Social Icons</h5>
									
									<?php include('inc/i-social.php'); ?>
									<div class="hovered">
										<?php include('inc/i-social.php'); ?>
									</div><!-- .hovered -->
								</div>
							</div><!-- .item -->
						</div><!-- .col -->
					</div><!-- .split-screen-grid -->
				</div><!-- .sw -->
			</section><!-- .light -->
			
			<!-- LISTS -->
			<section>
				<div class="sw">
					<h2>Lists</h2>
					
					<div class="grid eqh collapse-800">
						<div class="col-2 col">
							<div class="item">
								<div>
									<h5>Unordered List</h5>
								
									<ul>
										<li>
											Etiam accumsan hendrerit diam, quis ultricies sem. Sed imperdiet ipsum lorem, 
											vitae suscipit diam tempor ut. Phasellus eget massa.
										</li>
										<li>
											Etiam accumsan hendrerit diam, quis ultricies sem.
											<ul>
												<li>
													Etiam accumsan hendrerit diam, quis ultricies sem. Sed imperdiet ipsum lorem, 
													vitae suscipit diam tempor ut. Phasellus eget massa.
												</li>
												<li>
													Etiam accumsan hendrerit diam, quis ultricies sem.
												</li>
											</ul>
										</li>
										<li>
											Etiam accumsan hendrerit diam, quis ultricies sem.
										</li>
										<li>
											Etiam accumsan hendrerit diam, quis ultricies sem.
										</li>
									</ul>
								</div>
							</div><!-- .item -->
						</div><!-- .col -->
						<div class="col-2 col">
							<div class="item">
								<div>
									<h5>Ordered List</h5>
								
									<ol>
										<li>
											Etiam accumsan hendrerit diam, quis ultricies sem. Sed imperdiet ipsum lorem, 
											vitae suscipit diam tempor ut. Phasellus eget massa.
										</li>
										<li>
											Etiam accumsan hendrerit diam, quis ultricies sem.
											<ol>
												<li>
													Etiam accumsan hendrerit diam, quis ultricies sem. Sed imperdiet ipsum lorem, 
													vitae suscipit diam tempor ut. Phasellus eget massa.
												</li>
												<li>
													Etiam accumsan hendrerit diam, quis ultricies sem.
												</li>
											</ol>
										</li>
										<li>
											Etiam accumsan hendrerit diam, quis ultricies sem.
										</li>
										<li>
											Etiam accumsan hendrerit diam, quis ultricies sem.
										</li>
									</ol>
								</div>
							</div><!-- .item -->
						</div><!-- .col -->
						
						<div class="col-1 col">
							<div class="item">
								<div>
									<h2>Links</h2>
									
									<h5>(Inline only - inside paragraphs, blockquotes, lists, or with a class of "inline")</h5>
									
									<p>
									
										<a href="#link">a:link</a>
										
										<br />
										<br />
										
										<a href="#hovered" class="hover">a:hover</a>
										
										<br />
										<br />
										
										
										<a href="#" class="visited">a:visited</a>
									
									</p>
									
								</div>
							</div><!-- .item -->
						</div><!-- .col -->
						
					</div><!-- .grid -->

				</div><!-- .sw -->
			</section>
			
			
		</div><!-- .article-body -->	
	</div><!-- .body -->
			

<?php include('inc/i-footer.php'); ?>