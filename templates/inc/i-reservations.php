<div class="reservations <?php echo is_home() ? 'expanded' : ''; ?>">
	<div class="sw">
	
		<form action="/" method="post">
			<div class="fields-wrap">
				<fieldset class="grid pad5">

					<div class="col-1 col">

						<span class="date input fa fa-abs fa-calendar">
							<!-- data-staticdp appends the datepicker element to the form itself -->
							<input type="text" name="checkin" data-start="true" data-linked="#check-out" placeholder="Check In" data-staticdp="true" id="check-in">
						</span><!-- .date.input -->

					</div><!-- .col -->

					<div class="col-1 col">

						<span class="date input fa fa-abs fa-calendar">
							<input type="text" name="checkout" data-finish="true" data-linked="#check-in" placeholder="Check Out" data-staticdp="true" id="check-out">
						</span><!-- .date.input -->

					</div><!-- .col -->

				</fieldset><!-- .grid -->
				<fieldset class="grid pad5">

					<div class="col-2 col">

						<div class="selector fa fa-angle-down fa-abs">
							<select name="adults">
								<option value="1">1 Adult</option>
								<option value="2">2 Adults</option>
								<option value="3">3 Adults</option>
								<option value="4">4 Adults</option>
							</select>
							<span class="value">1 Adult</span>
						</div><!-- .selector -->
						
					</div><!-- .col -->

					<div class="col-2 col">
						
						<div class="selector fa fa-angle-down fa-abs">
							<select name="adults">
								<option value="0">0 Children</option>
								<option value="1">1 Child</option>
								<option value="2">2 Children</option>
								<option value="3">3 Children</option>
							</select>
							<span class="value">0 Children</span>
						</div><!-- .selector -->

					</div><!-- .col -->
					
				</fieldset><!-- .grid -->
				<fieldset class="grid pad5">
					<div class="col-1 col">
					
						<span class="h6-style">Room Type</span>

						<div>

							<div class="room-type">

								<div class="selector fa fa-angle-down fa-abs">
									<select name="room-type[]">
										<option value="standard" selected>Standard</option>
										<option value="suite">Suite</option>
										<option value="both">Both</option>
									</select>
									<span class="value">&nbsp;</span>
								</div><!-- .selector -->

								<label class="custom-input"> 
									<input type="radio"	name="fake-room-type" checked="checked" value="standard"><span class="fa fa-circle-o">Standard</span>
								</label>
								<label class="custom-input">
									<input type="radio"	name="fake-room-type" value="suite"><span class="fa fa-circle-o">Suite</span>
								</label>				
								<label class="custom-input">
									<input type="radio"	name="fake-room-type" value="both"><span class="fa fa-circle-o">Both</span>
								</label>

							</div><!-- .room-type -->

							<span class="add-room fa fa-plus-circle">Add Room</span>

						</div>

					</div><!-- .col -->

				</fieldset>
				<fieldset class="grid pad5">
					<div class="col-1 col">
					
						<span class="h6-style">Rate Type</span>

						<div class="rate-type">

							<div class="selector fa fa-angle-down fa-abs">
								<select name="rate-type">
									<option value="standard" selected>Standard</option>
									<option value="package">Package</option>
									<option value="both">Both</option>
								</select>
								<span class="value">&nbsp;</span>
							</div><!-- .selector -->						

							<label class="custom-input"> 
								<input type="radio" checked="checked" name="fake-rate-type" value="standard"><span class="fa fa-circle-o">Standard</span>
							</label>
							<label class="custom-input"> 
								<input type="radio"	name="fake-rate-type" value="package"><span class="fa fa-circle-o">Package</span>
							</label>				
							<label class="custom-input"> 
								<input type="radio"	name="fake-rate-type" value="both"><span class="fa fa-circle-o">Both</span>
							</label>

						</div>
					</div><!-- .col -->

				</fieldset>		

				<button class="button dark-bg">Check Availability &amp; Rates</button>
			</div><!-- .fields-wrap -->
			
			<span class="toggle fa fa-angle-down">Book Now</span>
		</form>
	
	</div><!-- .sw -->
</div><!-- .reservations -->