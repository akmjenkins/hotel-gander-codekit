			<footer>
				<div class="sw">

					<div class="footer-contact">

						<a href="#" class="logo"><img src="../assets/images/hotels/hotel-gander-white-color.svg" alt="Hotel Gander"></a>
					
						<address>
							Steele Hotels <br>
							123 Address Street <br>
							St. John's, NL  A1N 1N1
						</address>

						<div class="phones">
							<span class="block">
								<span>Phone</span>
								1 800 000 0000
							</span>
							<span class="block">
								<span>Fax</span>
								1 800 000 0000
							</span>							
						</div><!-- .phones -->

						<a href="#">info@steelehotels.com</a>

					</div><!-- .footer-contact -->

					<div class="footer-wrap">
						<div class="footer-nav">
							<ul>
								<li><a href="#">Accommodations</a></li>
								<li><a href="#">Dining</a></li>
								<li><a href="#">Getting Here</a></li>
								<li><a href="#">Meetings &amp; Events</a></li>
								<li><a href="#">Attractions</a></li>
								<li><a href="#">Reservations</a></li>
							</ul>

						</div><!-- .footer-nav -->

						<div class="footer-hotels">
							<span>Affiliated</span>
							<ul>
								<li><a href="#"><img src="../assets/images/hotels/jag-light.svg" alt="JAG Hotel St. John's"></a></li>
								<li><a href="#"><img src="../assets/images/hotels/the-capital-light.svg" alt="The Capital Hotel St. John's"></a></li>
								<li><a href="#"><img src="../assets/images/hotels/the-albatross-light.svg" alt="The Albatross Hotel Gander"></a></li>
								<li><a href="#"><img src="../assets/images/hotels/sinbads-light.svg" alt="Sinbad's Hotel & Suites"></a></li>
								<li><a href="#"><img src="../assets/images/hotels/irving-west-light.svg" alt="The Irving West Hotel Gander"></a></li>
								<li><a href="#"><img src="../assets/images/hotels/glynmill-inn-light.svg" alt="The Glynmill Inn"></a></li>
							</ul>
						</div><!-- .footer-hotels -->
					</div><!-- .footer-wrap -->
					
					<div class="copyright">
					
						<?php include('inc/i-social.php'); ?>
					
						<ul>
							<li>Copyright &copy; <?php echo date('Y'); ?> <a href="/">Steele Hotels</a></li>
							<li><a href="#">Sitemap</a></li>
							<li><a href="#">Feedback</a></li>						
							<li><a href="#">Legal</a></li>
						</ul>
						
						<a href="http://jac.co" rel="external" title="JAC. We Create." id="jac" class="replace">JAC. We Create.</a>
					</div><!-- .copyright -->
					
				</div><!-- .sw -->
			
			</footer><!-- .footer -->
		
		</div><!-- .page-wrapper -->

		<?php include('i-search-overlay.php'); ?>		
			
		<script>
			var templateJS = {
				templateURL: 'http://dev.me/gander-codekit/',
				CACHE_BUSTER: '<?php echo time(); ?>'	/* cache buster - set this to some unique string whenever an update is performed on js/css files, or when an admin is logged in */
			};
		</script>
		
		<script src="../assets/js/min/main-min.js?<?php echo time(); ?>"></script>
	</body>
</html>
