<div class="nav-wrap">
	<div class="nav">
		<div class="sw">

			<nav>
				<ul>
					<li>
						<a href="#">Accommodations</a>
					</li>
					<li>
						<a href="#">Dining</a>
					</li>
					<li>
						<a href="#">Getting Here</a>
					</li>
					<li>
						<a href="#">Meetings &amp; Events</a>
					</li>
					<li>
						<a href="#">Attractions</a>
						
						<div>

							<ul>
								<li><a href="#">Dining</a></li>
								<li><a href="#">Shopping</a></li>
								<li><a href="#">Attractions</a></li>
							</ul>
							
							<div class="link-meta">
							
								<div>
								
									<div class="meta">
										<span class="title">Sed adipiscing purus non ligula aliquam, quis tincidunt erat placerat. Nulla facilisis risus eget.</span>
										<a href="#" class="with-ico button fa fa-map-marker">View Map</a>
									</div><!-- .meta -->
								
									<div class="img" data-img="assets/images/temp/nav-1.jpg">&nbsp;</div>
									
								</div>
							
								<div>
								
									<div class="meta">
										<span class="title">Sed adipiscing purus non ligula aliquam, quis tincidunt erat placerat. Nulla facilisis risus eget.</span>
									</div><!-- .meta -->
								
									<div class="img" data-img="assets/images/temp/nav-2.jpg">&nbsp;</div>
									
								</div>
								
								<div>
								
									<div class="meta">
										<span class="title">Sed adipiscing purus non ligula aliquam, quis tincidunt erat placerat. Nulla facilisis risus eget.</span>
									</div><!-- .meta -->
								
									<div class="img" data-img="assets/images/temp/nav-1.jpg">&nbsp;</div>
									
								</div>
								
							</div><!-- .meta -->

						</div>
						
					</li>
					<li>
						<a href="#">Promotions</a>
					</li>
					<li>
						<a href="#">Reservations</a>
					</li>
				</ul>
			</nav>

			<button id="toggle-search" class="fa fa-abs fa-search toggle-search-form">Search</button>

			<div class="top">
				<div class="meta-nav">
					<a href="#">Home</a>
					<a href="#">Media</a>
					<a href="#">Accessibility</a>
					<a href="#">Contact</a>
					<a class="toggle-search-form">Search</a>
				</div><!-- .meta-nav -->

				<?php include('i-social.php'); ?>

			</div><!-- .top -->
			
		</div><!-- .sw -->
	</div><!-- .nav -->
</div><!-- .nav-wrap -->