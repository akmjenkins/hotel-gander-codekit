<?php

	//you can remove these, I only included it so I could simulate WordPress conditionals while I was building the template
	function is_home() {
		global $bodyclass;
		return preg_match('/home/',$bodyclass);
	}

	function is_404() {
		global $bodyclass;
		return preg_match('/error404/',$bodyclass);
	}
?>
<!doctype html>
<!--[if IE 8 ]>    <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en"> <!--<![endif]--> 

	<head>
		<title>Hotel Gander</title>
		<meta charset="utf-8">
		
		<?php include('api-keys.php'); ?>
		
		<!-- datepicker css -->
		<link rel="stylesheet" href="../assets/js/modules/datepicker/datepicker.css">

		<!-- font awesome -->
		<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
		
		<!-- lato -->
		<link href='http://fonts.googleapis.com/css?family=Lato:300,400,700,400italic,700italic' rel="stylesheet">		
		
		<!-- modernizr -->
		<script src="../bower_components/sprockets-modernizr/modernizr.js"></script>

		<!-- magnific popup -->
		<link rel="stylesheet" href="../bower_components/magnific-popup/dist/magnific-popup.css">
		<script src="../bower_components/magnific-popup/dist/jquery.magnific-popup.js" async defer></script>

		
		<!-- jQuery -->
		<!--[if IE 8 ]>
			<script src="../assets/js/jquery-legacy/jquery.min.js"></script>	
			<link rel="stylesheet" href="../assets/css/ie8.css?<?php echo time(); ?>">
		<![endif]--> 
		<!--[if (gte IE 9)|!(IE)]><!-->
			<script src="../bower_components/jquery/dist/jquery.min.js"></script>	
		<!--<![endif]--> 
		
		<!-- favicons -->
		<link rel="icon" type="image/x-icon"  href="../assets/images/favicons/favicon.ico">
		<link rel="icon" type="image/png"  href="../assets/images/favicons/favicon-32.png">
		<link rel="icon" href="../assets/images/favicons/favicon-32.png" sizes="32x32">
		<link rel="apple-touch-icon-precomposed" sizes="152x152" href="../assets/images/favicons/favicon-152.png">
		<link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/images/favicons/favicon-144.png">
		<link rel="apple-touch-icon-precomposed" sizes="120x120" href="../assets/images/favicons/favicon-120.png">
		<link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/images/favicons/favicon-114.png">
		<link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/images/favicons/favicon-144.png">
		<link rel="apple-touch-icon-precomposed" href="../assets/images/favicons/favicon-114.png">	
		<meta name="msapplication-TileColor" content="#000000">
		<meta name="msapplication-TileImage" content="../assets/images/favicons/favicon-144.png">
		
		<!-- place our CSS after all the other stylesheets so that we can more easily override their styles -->
		<link rel="stylesheet" href="../assets/css/style.css?<?php echo time(); ?>">		
		
	</head>
	<body class="<?php echo $bodyclass; ?>">
		<button id="mobile-nav" class="transform-button fa fa-abs fa-navicon">Mobile Nav</button>
		<button id="mobile-book" class="transform-button">Book Now</button>
	
		<?php include('inc/i-reservations.php'); ?>

		<?php include('i-nav.php'); ?>
	
		<div class="page-wrapper">	
			<header>
				<div class="sw">
					
					<a href="#" class="logo">
						<img src="../assets/images/hotels/hotel-gander-white-color.svg" alt="Hotel Gander">
					</a>
					
				</div><!-- .sw -->
			</header>
