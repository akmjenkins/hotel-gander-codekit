<div class="hero">
	<div class="swiper-wrapper">
		<div class="swipe" data-controls="true" data-auto="7">
			<div class="swipe-wrap">

				<div data-src="../assets/images/temp/hero/hero-1.jpg">
					<div class="item">&nbsp;</div>
					
					<div class="caption">
						<div class="sw">
							<span class="title">A Good Night Starts With Us</span>
							<span class="sub">Across the Island</span>

							<!-- 
								This view button wasn't in the original design,
								but it's useful on mobile (visible on mobile only)
								because the homepage is kind of bare except for the
								menu and book now button, so this gives the mobile user
								just one more thing to interact with above the fold

								Link it to the page for this hotel

							-->
							<a href="#" class="button dark-bg">More</a>

						</div><!-- .sw -->
					</div><!-- .caption -->
					
				</div>

				<div data-src="../assets/images/temp/hero/hero-2.jpg">
					<div class="item">&nbsp;</div>
					
					<div class="caption">
						<div class="sw">
							<span class="title">A Good Night Starts With Us</span>
							<span class="sub">Across the Island</span>

							<a href="#" class="button dark-bg">More</a>

						</div><!-- .sw -->
					</div><!-- .caption -->
					
				</div>

				<div data-src="../assets/images/temp/hero/hero-3.jpg">
					<div class="item">&nbsp;</div>
					
					<div class="caption">
						<div class="sw">
							<span class="title">A Good Night Starts With Us</span>
							<span class="sub">Across the Island</span>

							<a href="#" class="button dark-bg">More</a>
						</div><!-- .sw -->							
					</div><!-- .caption -->
					
				</div>

			</div><!-- .swipe-wrap -->
		</div><!-- .swipe -->
	</div><!-- .swiper-wrapper -->
</div><!-- .hero -->