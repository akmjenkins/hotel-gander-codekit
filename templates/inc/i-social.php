<div class="social">
	<a href="#" title="Like Brace for Performance on Facebook" class="fa fa-abs fa-facebook" rel="external">Like Brace for Performance on Facebook</a>
	<a href="#" title="Follow Brace for Performance on Twitter" class="fa fa-abs fa-twitter" rel="external">Follow Brace for Performance on Twitter</a>
	<a href="#" title="Check out Hotel Gander's Photos on Instagram" class="fa fa-abs fa-instagram" rel="external">Check out Hotel Gander's Photos on Instagram</a>
	<a href="#" title="Add Hotel Gander to Your Circle on Google Plus" class="fa fa-abs fa-google-plus" rel="external">Add Hotel Gander to Your Circle on Google Plus</a>
	<a href="#" title="Check out Hotel Gander's YouTube Channel" class="fa fa-abs fa-youtube" rel="external">Check out Hotel Gander's YouTube Channel</a>
</div><!-- .social -->