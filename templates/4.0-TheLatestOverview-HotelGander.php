<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

	<div class="hero single">
		<div class="swiper-wrapper">
			<div class="swipe" data-controls="true" data-auto="7">
				<div class="swipe-wrap">

					<div data-src="../assets/images/temp/hero/hero-inside-2.jpg">
						<div class="item">&nbsp;</div>
						
						<div class="caption">
							<div class="sw">
								<h1 class="title">The Latest</h1>
								<span class="sub">Aliquam Risus Eros</span>

							</div><!-- .sw -->
						</div><!-- .caption -->
					</div>
					
				</div><!-- .swipe-wrap -->
			</div><!-- .swipe -->
		</div><!-- .swiper-wrapper -->
	</div><!-- .hero -->			

	<div class="body">
	
		<div class="breadcrumbs">
			<div class="sw">
				<a href="#" class="fa fa-abs fa-home">Home</a>
				<a href="#">Media</a>
			</div><!-- .sw -->
		</div><!-- .breadcrumbs -->
		
		<section>
			<div class="sw">
				<div class="grid eqh collapse-no-flex blocks collapse-800">
				
					<div class="col-3 col">
					
						<div class="item with-title">
						
							<div class="hgroup">
								<h2 class="title">Latest News</h2>
								<span class="h5-style subtitle">Lorem ipsum dolor sit amet</span>
							</div><!-- .hgroup -->
						
							<a class="block with-img" href="#">
								<div class="img-wrap">
									<div class="img" style="background-image: url(../assets/images/temp/featured-promotion-block-1.jpg);"></div>
								</div><!-- .img-wrap -->
								<div class="content">
									<div class="hgroup">
										<h3 class="title">Morbi malesuada nibh non blandit semper</h3>
										<span class="subtitle h6-style">Etiam enim lorem, aliquam a iaculis</span>
									</div><!-- .hgroup -->
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.</p>
									<span class="button">Read More</span>
								</div><!-- .content -->
							</a><!-- .block -->
							
						</div><!-- .item -->
					</div><!-- .col-3 -->

					<div class="col-3 col">
						<div class="item with-title">
						
							<div class="hgroup">
								<h2 class="title">Latest Event</h2>
								<span class="h5-style subtitle">Lorem ipsum dolor sit amet</span>
							</div><!-- .hgroup -->
						
							<a class="block with-img" href="#">
								<div class="img-wrap">
									<div class="img" style="background-image: url(../assets/images/temp/featured-promotion-block-1.jpg);"></div>
								</div><!-- .img-wrap -->
								<div class="content">
									<div class="hgroup">
										<h3 class="title">Morbi malesuada nibh non blandit semper</h3>
									</div><!-- .hgroup -->
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod.</p>
									<span class="button">Read More</span>
								</div><!-- .content -->
							</a><!-- .block -->
							
						</div><!-- .item -->
					</div><!-- .col-3 -->

					<div class="col-3 col">
						<div class="item with-title">
						
							<div class="hgroup">
								<h2 class="title">Latest Promotion</h2>
								<span class="h5-style subtitle">Lorem ipsum dolor sit amet</span>
							</div><!-- .hgroup -->
						
							<a class="block with-img" href="#">
								<div class="img-wrap">
									<div class="img" style="background-image: url(../assets/images/temp/search-result-header-2.jpg);"></div>
								</div><!-- .img-wrap -->
								<div class="content">
									<div class="hgroup">
										<h3 class="title">Morbi malesuada</h3>
										<span class="subtitle h6-style">Etiam enim lorem, aliquam a iaculis</span>
									</div><!-- .hgroup -->

									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.</p>
									<span class="button">Read More</span>
								</div><!-- .content -->
							</a><!-- .block -->
							
						</div><!-- .item -->
					</div><!-- .col-3 -->
					
				</div><!-- .grid.eqh -->
			</div><!-- .sw -->
		</section>
		
		<section class="white">
			<div class="sw">
			
				<div class="grid latest-media-grid">
					<div class="col-2-3 col">
						<div class="item">
						
							<div class="hgroup">
								<h2>Latest Videos</h2>
								<h5 class="light">Lorem ipsum dolor sit amet</h5>
							</div><!-- .hgroup -->
							
							<div class="grid">
								<div class="col-2 col">
									<div>
										<a href="https://vimeo.com/105192180" data-title="Video #1" data-gallery="video-gallery" class="mpopup fa fa-abs fa-play-circle-o video pad-thumb landscape" style="background-image: url(../assets/images/temp/photos-videos/1.jpg);"></a>
									</div>
								</div><!-- .col-2 -->
								<div class="col-2 col">
									<div>
										<a href="https://www.youtube.com/watch?v=cfR7qxtgCgY" data-title="Video #2" data-gallery="video-gallery" class="mpopup fa fa-abs fa-play-circle-o video pad-thumb landscape" style="background-image: url(../assets/images/temp/photos-videos/2.jpg);"></a>
									</div>
								</div><!-- .col-2 -->
								<div class="col-2 col">
									<div>
										<a href="https://vimeo.com/105192180" data-title="Video #3" data-gallery="video-gallery" class="mpopup fa fa-abs fa-play-circle-o video pad-thumb landscape" style="background-image: url(../assets/images/temp/photos-videos/7.jpg);"></a>
									</div>
								</div><!-- .col-2 -->
								<div class="col-2 col">
									<div>
										<a href="https://www.youtube.com/watch?v=cfR7qxtgCgY" data-title="Video #4" data-gallery="video-gallery" class="mpopup fa fa-abs fa-play-circle-o video pad-thumb landscape" style="background-image: url(../assets/images/temp/photos-videos/5.jpg);"></a>
									</div>
								</div><!-- .col-2 -->
							</div><!-- .grid -->
							
							<a href="#" class="button right">View All Video</a>
							
						</div><!-- .item -->
					</div><!-- .col-2 -->
					<div class="col-3 col">
						<div class="item">
							
							<div class="hgroup">
								<h2>Latest Photos</h2>
								<h5 class="light">Lorem ipsum dolor sit amet</h5>
							</div><!-- .hgroup -->
							
							<div class="grid">
								<div class="col-2 col">
									<div>
										<a href="../assets/images/temp/photos-videos/1.jpg" data-title="Photo #1" data-gallery="photo-gallery" class="mpopup pad-thumb square" style="background-image: url(../assets/images/temp/photos-videos/1.jpg);"></a>
									</div>
								</div><!-- .col-2 -->
								<div class="col-2 col">
									<div>
										<a href="../assets/images/temp/photos-videos/2.jpg" data-title="Photo #2" data-gallery="photo-gallery" class="mpopup pad-thumb square" style="background-image: url(../assets/images/temp/photos-videos/2.jpg);"></a>
									</div>
								</div><!-- .col-2 -->
								<div class="col-2 col">
									<div>
										<a href="../assets/images/temp/photos-videos/7.jpg" data-title="Photo #3" data-gallery="photo-gallery" class="mpopup pad-thumb square" style="background-image: url(../assets/images/temp/photos-videos/7.jpg);"></a>
									</div>
								</div><!-- .col-2 -->
								<div class="col-2 col">
									<div>
										<a href="../assets/images/temp/photos-videos/5.jpg" data-title="Photo #4" data-gallery="photo-gallery" class="mpopup pad-thumb square" style="background-image: url(../assets/images/temp/photos-videos/5.jpg);"></a>
									</div>
								</div><!-- .col-2 -->
							</div><!-- .grid -->
							
							<a href="#" class="button right">View All Photos</a>
							
						</div><!-- .item -->
					</div><!-- .col -->
				</div><!-- .grid -->
			
			</div><!-- .sw -->
		</section><!-- .light -->

	
	</div><!-- .body -->


<?php include('inc/i-footer.php'); ?>