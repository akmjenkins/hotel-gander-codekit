<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

	<div class="hero single">
		<div class="swiper-wrapper">
			<div class="swipe" data-controls="true" data-auto="7">
				<div class="swipe-wrap">

					<div data-src="../assets/images/temp/hero/hero-inside-2.jpg">
						<div class="item">&nbsp;</div>
						
						<div class="caption">
							<div class="sw">
								<h1 class="title">Media</h1>
								<span class="sub">Aliquam Risus Eros.</span>

							</div><!-- .sw -->
						</div><!-- .caption -->
					</div>
					
				</div><!-- .swipe-wrap -->
			</div><!-- .swipe -->
		</div><!-- .swiper-wrapper -->
	</div><!-- .hero -->			

	<div class="body">
	
		<div class="breadcrumbs">
			<div class="sw">
				<a href="#" class="fa fa-abs fa-home">Home</a>
					<a href="#">Media</a>
			</div><!-- .sw -->
		</div><!-- .breadcrumbs -->
	
		<article>
			
			<div class="article-body">
			
				<section>
					<div class="sw">
							
						<p class="excerpt">
							Donec at augue nec ante hendrerit venenatis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Donec adipiscing ut sem tempus sodales. 
						</p>
						
						<p>
							Nullam malesuada leo in risus dictum ullamcorper. Fusce elementum, lorem vel varius aliquam, justo massa dignissim tortor, in tempor eros arcu nec ipsum. In tempus mattis libero, 
							sit amet placerat nisl ultrices in. Nulla a fermentum sem. Proin in diam ut enim tristique lobortis. Phasellus porta mollis erat, quis porttitor purus vehicula eu. Vestibulum sit amet 
							lectus magna. Nam et rhoncus turpis. Sed nec feugiat ligula. Donec at erat eros. Quisque eu convallis dui.
						</p>
							
					</div><!-- .sw -->
				</section><!-- .sw -->
				
				<section class="white">
					<div class="sw">
						
						<div class="section-header hgroup">
							<h2 class="title">Brand Guidelines</h2>
							<span class="subtitle h4-style">Aliquam Risus Eros</span>
						</div><!-- .hgroup -->
						
						<p>
							Nullam malesuada leo in risus dictum ullamcorper. Fusce elementum, lorem vel varius aliquam, justo massa dignissim tortor, in tempor eros arcu nec ipsum. In tempus mattis libero, sit amet placerat nisl ultrices in. Nulla a fermentum sem. Proin in diam ut enim tristique lobortis. Phasellus porta mollis erat, quis porttitor purus vehicula eu. Vestibulum sit amet lectus magna. Nam et rhoncus turpis. Sed nec feugiat ligula. Donec at erat eros. Quisque eu convallis dui.
						</p>
						
						<div class="center">
							<a href="#" class="button">Download Guidelines</a>
						</div>
						
					</div><!-- .sw -->
				</section><!-- .white -->
				
				<section class="blue-bg dark-bg">
					<div class="sw">
					
						<div class="section-header hgroup">
							<h2 class="title">Brand Guidelines</h2>
							<span class="subtitle h4-style">Aliquam Risus Eros</span>
						</div><!-- .hgroup -->
						
						<div class="grid">
						
							<div class="col col-4 sm-col-2 xs-col-1">
								<a class="item center" href="#">
								
									<div class="logo-wrap grey-bg">
										<img src="../assets/images/hotels/hotel-gander-color.svg" alt="Hotel Gander Logo Color">
									</div>
									
									<h3>Full Colour Logo</h3>
									<span class="button">Download</span>
									
								</a><!-- .item -->
							</div><!-- .col -->
							
							<div class="col col-4 sm-col-2 xs-col-1">
								<a class="item center" href="#">
								
									<div class="logo-wrap white">
										<img src="../assets/images/hotels/hotel-gander-greyscale.svg" alt="Hotel Gander Logo Greyscale">
									</div>
									
									<h3>Greyscale Logo</h3>
									<span class="button">Download</span>
									
								</a><!-- .item -->
							</div><!-- .col -->
							
							<div class="col col-4 sm-col-2 xs-col-1">
								<a class="item center" href="#">
								
									<div class="logo-wrap white">
										<img src="../assets/images/hotels/hotel-gander-black.svg" alt="Hotel Gander Logo Black">
									</div>
									
									<h3>Black Logo</h3>
									<span class="button">Download</span>
									
								</a><!-- .item -->
							</div><!-- .col -->
							
							<div class="col col-4 sm-col-2 xs-col-1">
								<a class="item center" href="#">
								
									<div class="logo-wrap transparent">
										<img src="../assets/images/hotels/hotel-gander-white.svg" alt="Hotel Gander Logo White">
									</div><!-- .logo-wrap -->
									
									<h3>White Logo</h3>
									<span class="button">Download</span>
									
								</a><!-- .item -->
							</div><!-- .col -->
							
						</div><!-- .grid -->
						
					</div><!-- .sw -->
				</section><!-- .dark-bg -->
				
				<section>
					<div class="sw">
					
						<div class="section-header hgroup">
							<h2 class="title">Media Contact</h2>
							<span class="subtitle h4-style">Aliquam Risus Eros</span>
						</div><!-- .hgroup -->
						
						<div class="grid">
							<div class="col-2 col sm-col-1">
								<div class="item">
									
									<h5>Media Contact</h5>
									
									<address>
										<strong>Media Contact Name</strong> <br />
										123 This Street, <br />
										This Town, NL  A1B 2C3
									</address>
									
									<br />
									<br />
									
									<div class="phones">
										<span class="block">
											<span>TF</span>
											1.888.555.5555
										</span>
										<span class="block">
											<span>PH</span>
											709.555.5555
										</span>
										<span class="block">
											<span>FAX</span>
											709.555.5555
										</span>
									</div><!-- .phones -->
									
								</div><!-- .item -->
							</div><!-- .col -->
							<div class="col-2 col sm-col-1">
							
								<div class="item">
									<form action="/" method="post" class="body-form contact-form">
										<fieldset>
											<input type="text" name="name" placeholder="Name">
											<input type="email" name="email" placeholder="Email">
											<input type="tel" pattern="\d+" name="phone" placeholder="Phone">
											<textarea name="message" cols="30" rows="10" placeholder="Message"></textarea>
											<button class="button right" type="submit">Send Message</button>
										</fieldset>
									</form><!-- .body-form -->
								</div><!-- .item -->
								
							</div><!-- .col -->
							
						</div><!-- .grid -->
						
						
					</div><!-- .sw -->
				</section>
		
			</div><!-- .article-body -->
		
		</article>
	
	</div><!-- .body -->
			

<?php include('inc/i-footer.php'); ?>