<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

	<div class="hero single">
		<div class="swiper-wrapper">
			<div class="swipe" data-controls="true" data-auto="7">
				<div class="swipe-wrap">

					<div data-src="../assets/images/temp/hero/hero-inside-2.jpg">
						<div class="item">&nbsp;</div>
						
						<div class="caption">
							<div class="sw">
								<h1 class="title">Photos &amp; Videos</h1>
								<span class="sub">Across the Island</span>

							</div><!-- .sw -->
						</div><!-- .caption -->
					</div>
					
				</div><!-- .swipe-wrap -->
			</div><!-- .swipe -->
		</div><!-- .swiper-wrapper -->
	</div><!-- .hero -->			

	<div class="body">
	
		<div class="breadcrumbs">
			<div class="sw">
				<a href="#" class="fa fa-abs fa-home">Home</a>
				<a href="#">The Latest</a>
				<a href="#">Photos &amp; Videos</a>
			</div><!-- .sw -->
		</div><!-- .breadcrumbs -->
		
		<div class="filter-section">
		
			<div class="tab-wrapper">
		
				<div class="filter-bar dark-bg tab-controls">
					<div class="sw">
						<div class="meta">
							
							<div class="controls">
								<button class="fa fa-abs fa-angle-left arrow-button previous">Prev</span>
								<button class="fa fa-abs fa-angle-right arrow-button next">Next</span>
							</div><!-- .controls -->
							
						</div><!-- .meta -->
					
						<div class="tab-control-wrap">
							<button class="tab-control selected">Photos</button>
							<button class="tab-control">Videos</button>
						</div>
					
					</div><!-- .sw -->
				</div><!-- .filter-bar -->
				
				<div class="tab-holder">
				
					<div class="tab selected">
				
						<div class="filter-contents">
							<div class="sw">
							
								<div class="grid">
									<div class="col-5 col sm-col-3 xs-col-2">
										<div>
											<a href="../assets/images/temp/photos-videos/1.jpg" data-tile="Photo #1" data-gallery="photo-gallery" class="mpopup pad-thumb square" style="background-image: url(../assets/images/temp/photos-videos/1.jpg);"></a>
										</div>
									</div><!-- .col -->
									<div class="col-5 col sm-col-3 xs-col-2">
										<div>
											<a href="../assets/images/temp/photos-videos/2.jpg"  data-title="Photo #2" data-gallery="photo-gallery" class="mpopup pad-thumb square" style="background-image: url(../assets/images/temp/photos-videos/2.jpg);"></a>
										</div>
									</div><!-- .col -->
									<div class="col-5 col sm-col-3 xs-col-2">
										<div>
											<a href="../assets/images/temp/photos-videos/7.jpg" data-title="Photo #3" data-gallery="photo-gallery" class="mpopup pad-thumb square" style="background-image: url(../assets/images/temp/photos-videos/7.jpg);"></a>
										</div>
									</div><!-- .col -->
									<div class="col-5 col sm-col-3 xs-col-2">
										<div>
											<a href="../assets/images/temp/photos-videos/5.jpg"   data-title="Photo #4" data-gallery="photo-gallery" class="mpopup pad-thumb square" style="background-image: url(../assets/images/temp/photos-videos/5.jpg);"></a>
										</div>
									</div><!-- .col -->
									<div class="col-5 col sm-col-3 xs-col-2">
										<div>
											<a href="../assets/images/temp/photos-videos/1.jpg" data-tile="Photo #5" data-gallery="photo-gallery" class="mpopup pad-thumb square" style="background-image: url(../assets/images/temp/photos-videos/1.jpg);"></a>
										</div>
									</div><!-- .col -->
									<div class="col-5 col sm-col-3 xs-col-2">
										<div>
											<a href="../assets/images/temp/photos-videos/1.jpg" data-tile="Photo #1" data-gallery="photo-gallery" class="mpopup pad-thumb square" style="background-image: url(../assets/images/temp/photos-videos/1.jpg);"></a>
										</div>
									</div><!-- .col -->
									<div class="col-5 col sm-col-3 xs-col-2">
										<div>
											<a href="../assets/images/temp/photos-videos/2.jpg"  data-title="Photo #2" data-gallery="photo-gallery" class="mpopup pad-thumb square" style="background-image: url(../assets/images/temp/photos-videos/2.jpg);"></a>
										</div>
									</div><!-- .col -->
									<div class="col-5 col sm-col-3 xs-col-2">
										<div>
											<a href="../assets/images/temp/photos-videos/7.jpg" data-title="Photo #3" data-gallery="photo-gallery" class="mpopup pad-thumb square" style="background-image: url(../assets/images/temp/photos-videos/7.jpg);"></a>
										</div>
									</div><!-- .col -->
									<div class="col-5 col sm-col-3 xs-col-2">
										<div>
											<a href="../assets/images/temp/photos-videos/5.jpg"   data-title="Photo #4" data-gallery="photo-gallery" class="mpopup pad-thumb square" style="background-image: url(../assets/images/temp/photos-videos/5.jpg);"></a>
										</div>
									</div><!-- .col -->
									<div class="col-5 col sm-col-3 xs-col-2">
										<div>
											<a href="../assets/images/temp/photos-videos/1.jpg" data-tile="Photo #5" data-gallery="photo-gallery" class="mpopup pad-thumb square" style="background-image: url(../assets/images/temp/photos-videos/1.jpg);"></a>
										</div>
									</div><!-- .col -->
									<div class="col-5 col sm-col-3 xs-col-2">
										<div>
											<a href="../assets/images/temp/photos-videos/1.jpg" data-tile="Photo #1" data-gallery="photo-gallery" class="mpopup pad-thumb square" style="background-image: url(../assets/images/temp/photos-videos/1.jpg);"></a>
										</div>
									</div><!-- .col -->
									<div class="col-5 col sm-col-3 xs-col-2">
										<div>
											<a href="../assets/images/temp/photos-videos/2.jpg"  data-title="Photo #2" data-gallery="photo-gallery" class="mpopup pad-thumb square" style="background-image: url(../assets/images/temp/photos-videos/2.jpg);"></a>
										</div>
									</div><!-- .col -->
									<div class="col-5 col sm-col-3 xs-col-2">
										<div>
											<a href="../assets/images/temp/photos-videos/7.jpg" data-title="Photo #3" data-gallery="photo-gallery" class="mpopup pad-thumb square" style="background-image: url(../assets/images/temp/photos-videos/7.jpg);"></a>
										</div>
									</div><!-- .col -->
									<div class="col-5 col sm-col-3 xs-col-2">
										<div>
											<a href="../assets/images/temp/photos-videos/5.jpg"   data-title="Photo #4" data-gallery="photo-gallery" class="mpopup pad-thumb square" style="background-image: url(../assets/images/temp/photos-videos/5.jpg);"></a>
										</div>
									</div><!-- .col -->
									<div class="col-5 col sm-col-3 xs-col-2">
										<div>
											<a href="../assets/images/temp/photos-videos/1.jpg" data-tile="Photo #5" data-gallery="photo-gallery" class="mpopup pad-thumb square" style="background-image: url(../assets/images/temp/photos-videos/1.jpg);"></a>
										</div>
									</div><!-- .col -->
								</div><!-- .grid -->

							</div><!-- .sw -->
						</div><!-- .filter-contents -->
						
					</div><!-- .tab -->
					
					<div class="tab">
					
						<div class="filter-contents">
							<div class="sw">
							
								<div class="grid">
									<div class="col-3 col sm-col-2 xs-col-1">
										<div>
											<a href="https://vimeo.com/105192180" data-title="Video #1" data-gallery="video-gallery" class="mpopup fa fa-abs fa-play-circle-o video pad-thumb landscape" style="background-image: url(../assets/images/temp/photos-videos/1.jpg);"></a>
										</div>
									</div><!-- .col -->
									<div class="col-3 col sm-col-2 xs-col-1">
										<div>
											<a href="https://www.youtube.com/watch?v=cfR7qxtgCgY" data-title="Video #2" data-gallery="video-gallery" class="mpopup fa fa-abs fa-play-circle-o video pad-thumb landscape" style="background-image: url(../assets/images/temp/photos-videos/2.jpg);"></a>
										</div>
									</div><!-- .col -->
									<div class="col-3 col sm-col-2 xs-col-1">
										<div>
											<a href="https://vimeo.com/105192180" data-title="Video #3" data-gallery="video-gallery" class="mpopup fa fa-abs fa-play-circle-o video pad-thumb landscape" style="background-image: url(../assets/images/temp/photos-videos/7.jpg);"></a>
										</div>
									</div><!-- .col -->
									<div class="col-3 col sm-col-2 xs-col-1">
										<div>
											<a href="https://www.youtube.com/watch?v=cfR7qxtgCgY" data-title="Video #4" data-gallery="video-gallery" class="mpopup fa fa-abs fa-play-circle-o video pad-thumb landscape" style="background-image: url(../assets/images/temp/photos-videos/5.jpg);"></a>
										</div>
									</div><!-- .col -->
									<div class="col-3 col sm-col-2 xs-col-1">
										<div>
											<a href="https://vimeo.com/105192180" data-title="Video #5" data-gallery="video-gallery" class="mpopup fa fa-abs fa-play-circle-o video pad-thumb landscape" style="background-image: url(../assets/images/temp/photos-videos/1.jpg);"></a>
										</div>
									</div><!-- .col -->
									<div class="col-3 col sm-col-2 xs-col-1">
										<div>
											<a href="https://vimeo.com/105192180" data-title="Video #1" data-gallery="video-gallery" class="mpopup fa fa-abs fa-play-circle-o video pad-thumb landscape" style="background-image: url(../assets/images/temp/photos-videos/1.jpg);"></a>
										</div>
									</div><!-- .col -->
									<div class="col-3 col sm-col-2 xs-col-1">
										<div>
											<a href="https://www.youtube.com/watch?v=cfR7qxtgCgY" data-title="Video #2" data-gallery="video-gallery" class="mpopup fa fa-abs fa-play-circle-o video pad-thumb landscape" style="background-image: url(../assets/images/temp/photos-videos/2.jpg);"></a>
										</div>
									</div><!-- .col -->
									<div class="col-3 col sm-col-2 xs-col-1">
										<div>
											<a href="https://vimeo.com/105192180" data-title="Video #3" data-gallery="video-gallery" class="mpopup fa fa-abs fa-play-circle-o video pad-thumb landscape" style="background-image: url(../assets/images/temp/photos-videos/7.jpg);"></a>
										</div>
									</div><!-- .col -->
									<div class="col-3 col sm-col-2 xs-col-1">
										<div>
											<a href="https://www.youtube.com/watch?v=cfR7qxtgCgY" data-title="Video #4" data-gallery="video-gallery" class="mpopup fa fa-abs fa-play-circle-o video pad-thumb landscape" style="background-image: url(../assets/images/temp/photos-videos/5.jpg);"></a>
										</div>
									</div><!-- .col -->
									<div class="col-3 col sm-col-2 xs-col-1">
										<div>
											<a href="https://vimeo.com/105192180" data-title="Video #5" data-gallery="video-gallery" class="mpopup fa fa-abs fa-play-circle-o video pad-thumb landscape" style="background-image: url(../assets/images/temp/photos-videos/1.jpg);"></a>
										</div>
									</div><!-- .col -->
									<div class="col-3 col sm-col-2 xs-col-1">
										<div>
											<a href="https://vimeo.com/105192180" data-title="Video #1" data-gallery="video-gallery" class="mpopup fa fa-abs fa-play-circle-o video pad-thumb landscape" style="background-image: url(../assets/images/temp/photos-videos/1.jpg);"></a>
										</div>
									</div><!-- .col -->
									<div class="col-3 col sm-col-2 xs-col-1">
										<div>
											<a href="https://www.youtube.com/watch?v=cfR7qxtgCgY" data-title="Video #2" data-gallery="video-gallery" class="mpopup fa fa-abs fa-play-circle-o video pad-thumb landscape" style="background-image: url(../assets/images/temp/photos-videos/2.jpg);"></a>
										</div>
									</div><!-- .col -->
									<div class="col-3 col sm-col-2 xs-col-1">
										<div>
											<a href="https://vimeo.com/105192180" data-title="Video #3" data-gallery="video-gallery" class="mpopup fa fa-abs fa-play-circle-o video pad-thumb landscape" style="background-image: url(../assets/images/temp/photos-videos/7.jpg);"></a>
										</div>
									</div><!-- .col -->
									<div class="col-3 col sm-col-2 xs-col-1">
										<div>
											<a href="https://www.youtube.com/watch?v=cfR7qxtgCgY" data-title="Video #4" data-gallery="video-gallery" class="mpopup fa fa-abs fa-play-circle-o video pad-thumb landscape" style="background-image: url(../assets/images/temp/photos-videos/5.jpg);"></a>
										</div>
									</div><!-- .col -->
									<div class="col-3 col sm-col-2 xs-col-1">
										<div>
											<a href="https://vimeo.com/105192180" data-title="Video #5" data-gallery="video-gallery" class="mpopup fa fa-abs fa-play-circle-o video pad-thumb landscape" style="background-image: url(../assets/images/temp/photos-videos/1.jpg);"></a>
										</div>
									</div><!-- .col -->
								</div><!-- .grid -->
							
							</div><!-- .sw -->
						</div><!-- .filter-contents -->
						
					</div><!-- .tab -->
				
				</div><!-- .tab-holder -->
			</div><!-- .tab-wrapper -->
			
	</div><!-- .filter-section -->
				
	</div><!-- .body -->
			

<?php include('inc/i-footer.php'); ?>