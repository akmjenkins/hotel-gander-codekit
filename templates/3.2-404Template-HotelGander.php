<?php $bodyclass = 'error404'; ?>
<?php include('inc/i-header.php'); ?>

	<div class="hero single">
		<div class="swiper-wrapper">
			<div class="swipe" data-controls="true" data-auto="7">
				<div class="swipe-wrap">

					<div data-src="../assets/images/temp/hero/hero-inside-2.jpg">
						<div class="item">&nbsp;</div>
						
						<div class="caption">
							<div class="sw">
								<h1 class="title">404 Error</h1>
								<span class="sub">Aliquam Risus Eros.</span>

							</div><!-- .sw -->
						</div><!-- .caption -->
					</div>
					
				</div><!-- .swipe-wrap -->
			</div><!-- .swipe -->
		</div><!-- .swiper-wrapper -->
	</div><!-- .hero -->			

	<div class="body">
	
		<div class="breadcrumbs">
			<div class="sw">
				<a href="#" class="fa fa-abs fa-home">Home</a>
					<a href="#">404 Error</a>
			</div><!-- .sw -->
		</div><!-- .breadcrumbs -->
			
		<section class="sw cf">
			<div class="main-body">
				<div class="article-body">
					
					<div class="center">
						<p class="excerpt">
							Donec at augue nec ante hendrerit venenatis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Donec adipiscing ut sem tempus sodales. 
						</p>
						
						<img src="../assets/images/temp/featured-promotion.jpg" alt="image"/>
					</div>
					
				</div><!-- .article-body -->
			</div><!-- .main-body -->
		</section><!-- .sw -->

	
	</div><!-- .body -->
			

<?php include('inc/i-footer.php'); ?>