<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

	<div class="hero single">
		<div class="swiper-wrapper">
			<div class="swipe" data-controls="true" data-auto="7">
				<div class="swipe-wrap">

					<div data-src="../assets/images/temp/hero/hero-inside-2.jpg">
						<div class="item">&nbsp;</div>
						
						<div class="caption">
							<div class="sw">
								<h1 class="title">Promotions</h1>
								<span class="sub">Aliquam Risus Eros</span>

							</div><!-- .sw -->
						</div><!-- .caption -->
					</div>
					
				</div><!-- .swipe-wrap -->
			</div><!-- .swipe -->
		</div><!-- .swiper-wrapper -->
	</div><!-- .hero -->			

	<div class="body">
	
		<div class="breadcrumbs">
			<div class="sw">
				<a href="#" class="fa fa-abs fa-home">Home</a>
				<a href="#">Promotions</a>
			</div><!-- .sw -->
		</div><!-- .breadcrumbs -->
		
		<section class="white">
			<div class="sw">
				
					<div class="hgroup">
						<h2 class="title">Featured Promotion</h2>
						<span class="h5-style subtitle">Quisque feugiat mauris mi ac fringilla</span>
					</div><!-- .hgroup -->
					
					<div class="featured-block">
						<img src="../assets/images/temp/top-search-result.jpg" alt="Promotion"/>
						
						<div class="content article-body">
							<div class="article-head">
								<span class="tag">Promotion</span>
								Ends September 21, 2014
							</div><!-- .article-head -->
							
							<div class="hgroup">
								<h2 class="title">Nullam a ligula eget velit</h2>
								<span class="h5-style subtitle">Quisque feugiat mauris miac fringilla</span>
							</div><!-- .hgroup -->
							
							<p>Quisque feugiat mauris mi, ac fringilla erat rutrum non. Morbi consequat massa in massa euismod, ac suscipit sem aliquam. 
							Sed libero felis, feugiat eu hendrerit sit amet, tincidunt gravida purus. Aenean aliquam erat a tincidunt vestibulum. 
							Curabitur placerat lacus at risus ornare convallis. </p>
							
							<a href="#" class="button">Read More</a>

						</div><!-- .content -->
						
					</div><!-- .featured-block -->

				
			</div><!-- .sw -->
		</section><!-- .white -->
	
		<section class="filter-section">
			<div class="filter-bar dark-bg">
				<div class="sw">
					<div class="meta">
					
						<div class="selector fa fa-angle-down fa-abs">
							<select name="sort-by">
								<option>Promotions</option>
							</select>
							<span class="value">&nbsp;</span>
						</div><!-- .selector -->
					
						<form action="/" method="post" class="search-form single-form">
							<fieldset>
								<input type="text" name="s" placeholder="Search news...">
								<button type="submit" class="fa fa-abs fa-search">Search</button>
							</fieldset>
						</form>
					
						<div class="controls">
							<button class="fa fa-abs fa-angle-left arrow-button previous">Prev</span>
							<button class="fa fa-abs fa-angle-right arrow-button next">Next</span>
						</div><!-- .controls -->
						
					</div><!-- .meta -->
				
					<span class="h6-style title">8 Promotions &amp; Packages</span>
				
				</div><!-- .sw -->
			</div><!-- .filter-bar -->
			
			<div class="filter-contents">
				<div class="sw">
				
					<div class="article-body">
						<div class="grid eqh collapse-no-flex blocks collapse-800">
						
							<div class="col-3 col">
								<div class="item">
								
									<a class="block with-img" href="#">
										<div class="img-wrap">
											<div class="img" style="background-image: url(../assets/images/temp/featured-promotion-block-1.jpg);"></div>
										</div><!-- .img-wrap -->
										<div class="content">
											<div class="article-head">
												<time datetime="2015-10-23">September 23, 2015</time>
											</div>
											<div class="hgroup">
												<h3 class="title">Morbi malesuada nibh non blandit semper</h3>
												<span class="h6-style subtitle">Etiam enim lorem, aliquam a iaculis</span>
											</div><!-- .hgroup -->
											<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.</p>
											<span class="button">Read More</span>
										</div><!-- .content -->
									</a><!-- .block -->
									
								</div><!-- .item -->
							</div><!-- .col -->

							<div class="col-3 col">
								<div class="item">
								
									<a class="block with-img" href="#">
										<div class="img-wrap">
											<div class="img" style="background-image: url(../assets/images/temp/search-result-header.jpg);"></div>
										</div><!-- .img-wrap -->
										<div class="content">
											<div class="article-head">
												<time datetime="2015-10-23">September 23, 2015</time>
											</div>
											<div class="hgroup">
												<h3 class="title">Morbi malesuada nibh non blandit semper</h3>
												<span class="h6-style subtitle">Etiam enim lorem, aliquam a iaculis</span>
											</div><!-- .hgroup -->
											<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.</p>
											<span class="button">Read More</span>
										</div><!-- .content -->
									</a><!-- .block -->
									
								</div><!-- .item -->
							</div><!-- .col -->

							<div class="col-3 col">
								<div class="item">
								
									<a class="block with-img" href="#">
										<div class="img-wrap">
											<div class="img" style="background-image: url(../assets/images/temp/search-result-header-2.jpg);"></div>
										</div><!-- .img-wrap -->
										<div class="content">
											<div class="article-head">
												<time datetime="2015-10-23">September 23, 2015</time>
											</div>
											<div class="hgroup">
												<h3 class="title">Morbi malesuada nibh non blandit semper</h3>
												<span class="h6-style subtitle">Etiam enim lorem, aliquam a iaculis</span>
											</div><!-- .hgroup -->
											<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.</p>
											<span class="button">Read More</span>
										</div><!-- .content -->
									</a><!-- .block -->
									
								</div><!-- .item -->
							</div><!-- .col -->
							
						</div><!-- .grid.eqh -->
					</div><!-- .article-body -->
				
				</div><!-- .sw -->
			</div><!-- .filter-contents -->
		</section><!-- .filter-section -->
		
		<section class="light split-screen article-body collapse-800 with-border">
			<div class="sw">
				<div class="grid split-screen-grid collapse-800">
				
					<div class="col-2 col">
						<div class="item">
							
							<div class="hgroup">
								<h2 class="title">Reward Programs</h2>
								<span class="h5-style subtitle">Quisque feugiat mauris mi ac fringilla</span>
							</div><!-- .hgroup -->
							
							<div class="grid eqh reward-partners">
								<div class="col-2 col">
									<div class="item center vcenter">
										<a href="#">
											<img src="../assets/images/temp/reward-program-steele.png" class="block" alt="Platinum Steele Program"/>
											<span class="button">More Info</span>
										</a><!-- .valign -->
									</div>
								</div>
								<div class="col-2 col">
									<div class="item center vcenter">
										<a href="#">
											<img src="../assets/images/temp/reward-program-aeroplan.png" class="block" alt="Aeroplan"/>
											<span class="button">More Info</span>
										</a><!-- .valign -->
									</div>
								</div>
							</div><!-- .grid -->
								
							
						</div>
					</div><!-- .col -->
					
					<div class="col-2 col">
						<div class="item">
						
							<div class="hgroup">
								<h2 class="title">Government &amp; Corporate Rates</h2>
								<span class="h5-style subtitle">Quisque feugiat mauris mi ac fringilla</span>
							</div><!-- .hgroup -->
							
							<p>
								Nulla auctor quis lorem ac adipiscing. Maecenas convallis et velit ac posuere. Duis dictum felis sit amet dui sollicitudin, 
								eget posuere sapien viverra. Cras porttitor ornare odio, eu faucibus lorem convallis eu. Etiam ac cursus nisl. 
								In nec hendrerit felis. Cras id sem at ante euismod semper. Sed placerat nisi tellus. Integer enim ipsum, 
								suscipit non aliquam a, pulvinar eget turpis.
							</p>
							
							<a href="#" class="button">More Info</a>
							
						</div>
					</div><!-- .col -->
					
				</div><!-- .grid -->
			</div><!-- .sw -->
		</section>
		
		<section>
			<div class="sw">
			
				<div class="hgroup">
					<h2 class="title">Partners &amp; Affiliates</h2>
					<span class="h5-style subtitle">Quisque feugiat mauris mi ac fringilla</span>
				</div><!-- .hgroup -->
				
				<div class="center partner-wrap">
					<a href="#" class="partner" rel="external" style="background-image: url(https://pbs.twimg.com/profile_images/1285433132/gravatar_200x200.jpeg);">
						<span><span>Visit Site</span></span>
					</a>
					<a href="#" class="partner" rel="external" style="background-image: none">
						<span><span>Visit Site</span></span>
					</a>
					<a href="#" class="partner" rel="external" style="background-image: none">
						<span><span>Visit Site</span></span>
					</a>
					<a href="#" class="partner" rel="external" style="background-image: none">
						<span><span>Visit Site</span></span>
					</a>
					<a href="#" class="partner" rel="external" style="background-image: none">
						<span><span>Visit Site</span></span>
					</a>
					<a href="#" class="partner" rel="external" style="background-image: none">
						<span><span>Visit Site</span></span>
					</a>
					<a href="#" class="partner" rel="external" style="background-image: none">
						<span><span>Visit Site</span></span>
					</a>
				</div><!-- .centered -->
			
				
			
			</div><!-- .sw -->
		</section>
		
	</div><!-- .body -->
			

<?php include('inc/i-footer.php'); ?>