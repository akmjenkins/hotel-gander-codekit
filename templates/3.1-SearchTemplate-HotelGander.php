<?php $bodyclass = 'search'; ?>
<?php include('inc/i-header.php'); ?>

	<div class="hero single">
		<div class="swiper-wrapper">
			<div class="swipe" data-controls="true" data-auto="7">
				<div class="swipe-wrap">

					<div data-src="../assets/images/temp/hero/hero-inside-2.jpg">
						<div class="item">&nbsp;</div>
						
						<div class="caption">
							<div class="sw">
								<h1 class="title">News</h1>
								<span class="sub">Aliquam Risus Eros</span>

							</div><!-- .sw -->
						</div><!-- .caption -->
					</div>
					
				</div><!-- .swipe-wrap -->
			</div><!-- .swipe -->
		</div><!-- .swiper-wrapper -->
	</div><!-- .hero -->			

	<div class="body">
	
		<div class="breadcrumbs">
			<div class="sw">
				<a href="#" class="fa fa-abs fa-home">Home</a>
				<a href="#">The Latest</a>
				<a href="#">News</a>
			</div><!-- .sw -->
		</div><!-- .breadcrumbs -->
	
		<section class="filter-section">
			<div class="filter-bar dark-bg">
				<div class="sw">
					<div class="meta">
					
						<div class="controls">
							<button class="fa fa-abs fa-angle-left arrow-button previous">Prev</span>
							<button class="fa fa-abs fa-angle-right arrow-button next">Next</span>
						</div><!-- .controls -->
						
					</div><!-- .meta -->
				
					<span class="h6-style title">
						<span class="cat">News Results</span>
						5 Articles Found
					</span><!-- .title -->
				
				</div><!-- .sw -->
			</div><!-- .filter-bar -->
			
			<div class="filter-contents">
				<div class="sw">
				
					<div class="article-body">
						<div class="grid eqh collapse-no-flex blocks collapse-800">
						
							<div class="col-3 col">
								<div class="item">
								
									<a class="block with-img" href="#">
										<div class="img-wrap">
											<div class="img" style="background-image: url(../assets/images/temp/featured-promotion-block-1.jpg);"></div>
										</div><!-- .img-wrap -->
										<div class="content">
											<div class="article-head">
												<time datetime="2015-10-23">September 23, 2015</time>
											</div>
											<div class="hgroup">
												<h3 class="title">Morbi malesuada nibh non blandit semper</h3>
												<span class="subtitle h6-style">Etiam enim lorem, aliquam a iaculis</span>
											</div><!-- .hgroup -->
											<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.</p>
											<span class="button">Read More</span>
										</div><!-- .content -->
									</a><!-- .block -->
									
								</div><!-- .item -->
							</div><!-- .col -->

							<div class="col-3 col">
								<div class="item">
								
									<a class="block with-img" href="#">
										<div class="img-wrap">
											<div class="img" style="background-image: url(../assets/images/temp/search-result-header.jpg);"></div>
										</div><!-- .img-wrap -->
										<div class="content">
											<div class="article-head">
												<time datetime="2015-10-23">September 23, 2015</time>
											</div>
											<div class="hgroup">
												<h3 class="title">Morbi malesuada nibh non blandit semper</h3>
												<span class="subtitle h6-style">Etiam enim lorem, aliquam a iaculis</span>
											</div><!-- .hgroup -->
											<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.</p>
											<span class="button">Read More</span>
										</div><!-- .content -->
									</a><!-- .block -->
									
								</div><!-- .item -->
							</div><!-- .col -->

							<div class="col-3 col">
								<div class="item">
								
									<a class="block with-img" href="#">
										<div class="img-wrap">
											<div class="img" style="background-image: url(../assets/images/temp/search-result-header-2.jpg);"></div>
										</div><!-- .img-wrap -->
										<div class="content">
											<div class="article-head">
												<time datetime="2015-10-23">September 23, 2015</time>
											</div>
											<div class="hgroup">
												<h3 class="title">Morbi malesuada nibh non blandit semper</h3>
												<span class="subtitle h6-style">Etiam enim lorem, aliquam a iaculis</span>
											</div><!-- .hgroup -->
											<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.</p>
											<span class="button">Read More</span>
										</div><!-- .content -->
									</a><!-- .block -->
									
								</div><!-- .item -->
							</div><!-- .col -->
							
						</div><!-- .grid.eqh -->
					</div><!-- .article-body -->
				
				</div><!-- .sw -->
			</div><!-- .filter-contents -->
		</section><!-- .filter-section -->

		<section class="filter-section">
			<div class="filter-bar dark-bg">
				<div class="sw">
					<div class="meta">
					
						<div class="controls">
							<button class="fa fa-abs fa-angle-left arrow-button previous">Prev</span>
							<button class="fa fa-abs fa-angle-right arrow-button next">Next</span>
						</div><!-- .controls -->
						
					</div><!-- .meta -->
				
					<span class="h6-style title">
						<span class="cat">Event Results</span>
						3 Events Found
					</span><!-- .title -->
				
				</div><!-- .sw -->
			</div><!-- .filter-bar -->
			
			<div class="filter-contents">
				<div class="sw">
				
					<div class="article-body">
						<div class="grid eqh collapse-no-flex blocks collapse-800">
						
							<div class="col-3 col">
								<div class="item">
								
									<a class="block with-img" href="#">
										<div class="img-wrap">
											<div class="img" style="background-image: url(../assets/images/temp/featured-promotion-block-1.jpg);"></div>
										</div><!-- .img-wrap -->
										<div class="content">
											<div class="article-head">
												<time datetime="2015-10-23">September 23, 2015</time>
											</div>
											<div class="hgroup">
												<h3 class="title">Morbi malesuada nibh non blandit semper</h3>
												<span class="subtitle h6-style">Etiam enim lorem, aliquam a iaculis</span>
											</div><!-- .hgroup -->
											<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.</p>
											<span class="button">Read More</span>
										</div><!-- .content -->
									</a><!-- .block -->
									
								</div><!-- .item -->
							</div><!-- .col -->

							<div class="col-3 col">
								<div class="item">
								
									<a class="block with-img" href="#">
										<div class="img-wrap">
											<div class="img" style="background-image: url(../assets/images/temp/search-result-header.jpg);"></div>
										</div><!-- .img-wrap -->
										<div class="content">
											<div class="article-head">
												<time datetime="2015-10-23">September 23, 2015</time>
											</div>
											<div class="hgroup">
												<h3 class="title">Morbi malesuada nibh non blandit semper</h3>
												<span class="subtitle h6-style">Etiam enim lorem, aliquam a iaculis</span>
											</div><!-- .hgroup -->
											<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.</p>
											<span class="button">Read More</span>
										</div><!-- .content -->
									</a><!-- .block -->
									
								</div><!-- .item -->
							</div><!-- .col -->

							<div class="col-3 col">
								<div class="item">
								
									<a class="block with-img" href="#">
										<div class="img-wrap">
											<div class="img" style="background-image: url(../assets/images/temp/search-result-header-2.jpg);"></div>
										</div><!-- .img-wrap -->
										<div class="content">
											<div class="article-head">
												<time datetime="2015-10-23">September 23, 2015</time>
											</div>
											<div class="hgroup">
												<h3 class="title">Morbi malesuada nibh non blandit semper</h3>
												<span class="subtitle h6-style">Etiam enim lorem, aliquam a iaculis</span>
											</div><!-- .hgroup -->
											<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.</p>
											<span class="button">Read More</span>
										</div><!-- .content -->
									</a><!-- .block -->
									
								</div><!-- .item -->
							</div><!-- .col -->
							
						</div><!-- .grid.eqh -->
					</div><!-- .article-body -->
				
				</div><!-- .sw -->
			</div><!-- .filter-contents -->
		</section><!-- .filter-section -->

		<section class="filter-section">
			<div class="filter-bar dark-bg">
				<div class="sw">
					<div class="meta">
					
						<div class="controls">
							<button class="fa fa-abs fa-angle-left arrow-button previous">Prev</span>
							<button class="fa fa-abs fa-angle-right arrow-button next">Next</span>
						</div><!-- .controls -->
						
					</div><!-- .meta -->
				
					<span class="h6-style title">
						<span class="cat">Page Results</span>
						2 Pages Found
					</span><!-- .title -->
				
				</div><!-- .sw -->
			</div><!-- .filter-bar -->
			
			<div class="filter-contents">
				<div class="sw">
				
					<div class="article-body">
						<div class="grid eqh collapse-no-flex blocks collapse-800">
						
							<div class="col-3 col">
								<div class="item">
								
									<a class="block with-img" href="#">
										<div class="img-wrap">
											<div class="img" style="background-image: url(../assets/images/temp/featured-promotion-block-1.jpg);"></div>
										</div><!-- .img-wrap -->
										<div class="content">
											<div class="article-head">
												<time datetime="2015-10-23">September 23, 2015</time>
											</div>
											<div class="hgroup">
												<h3 class="title">Morbi malesuada nibh non blandit semper</h3>
												<span class="subtitle h6-style">Etiam enim lorem, aliquam a iaculis</span>
											</div><!-- .hgroup -->
											<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.</p>
											<span class="button">Read More</span>
										</div><!-- .content -->
									</a><!-- .block -->
									
								</div><!-- .item -->
							</div><!-- .col -->

							<div class="col-3 col">
								<div class="item">
								
									<a class="block with-img" href="#">
										<div class="img-wrap">
											<div class="img" style="background-image: url(../assets/images/temp/search-result-header.jpg);"></div>
										</div><!-- .img-wrap -->
										<div class="content">
											<div class="article-head">
												<time datetime="2015-10-23">September 23, 2015</time>
											</div>
											<div class="hgroup">
												<h3 class="title">Morbi malesuada nibh non blandit semper</h3>
												<span class="subtitle h6-style">Etiam enim lorem, aliquam a iaculis</span>
											</div><!-- .hgroup -->
											<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.</p>
											<span class="button">Read More</span>
										</div><!-- .content -->
									</a><!-- .block -->
									
								</div><!-- .item -->
							</div><!-- .col -->
							
						</div><!-- .grid.eqh -->
					</div><!-- .article-body -->
				
				</div><!-- .sw -->
			</div><!-- .filter-contents -->
		</section><!-- .filter-section -->
		
		<section class="filter-section">
			<div class="filter-bar dark-bg">
				<div class="sw">
					<div class="meta">
					
						<div class="controls">
							<button class="fa fa-abs fa-angle-left arrow-button previous">Prev</span>
							<button class="fa fa-abs fa-angle-right arrow-button next">Next</span>
						</div><!-- .controls -->
						
					</div><!-- .meta -->
				
					<span class="h6-style title">
						<span class="cat">Promotion Results</span>
						2 Promotions Found
					</span><!-- .title -->
				
				</div><!-- .sw -->
			</div><!-- .filter-bar -->
			
			<div class="filter-contents">
				<div class="sw">
				
					<div class="article-body">
						<div class="grid eqh collapse-no-flex blocks collapse-800">
						
							<div class="col-3 col">
								<div class="item">
								
									<a class="block with-img" href="#">
										<div class="img-wrap">
											<div class="img" style="background-image: url(../assets/images/temp/search-result-header-2.jpg);"></div>
										</div><!-- .img-wrap -->
										<div class="content">
										
											<div class="article-head">
												<span class="tag">Promotion</span>
												Ends September 21, 2014
											</div><!-- .article-head -->
										
											<div class="hgroup">
												<h3 class="title">Morbi malesuada nibh non blandit semper</h3>
												<span class="subtitle h6-style">Etiam enim lorem, aliquam a iaculis</span>
											</div><!-- .hgroup -->
											<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.</p>
											<span class="button">Read More</span>
										</div><!-- .content -->
									</a><!-- .block -->
									
								</div><!-- .item -->
							</div><!-- .col -->

							<div class="col-3 col">
								<div class="item">
								
									<a class="block with-img" href="#">
										<div class="img-wrap">
											<div class="img" style="background-image: url(../assets/images/temp/search-result-header-2.jpg);"></div>
										</div><!-- .img-wrap -->
										<div class="content">
										
											<div class="article-head">
												<span class="tag">Promotion</span>
												Ends September 21, 2014
											</div><!-- .article-head -->
										
											<div class="hgroup">
												<h3 class="title">Morbi malesuada nibh non blandit semper</h3>
												<span class="subtitle h6-style">Etiam enim lorem, aliquam a iaculis</span>
											</div><!-- .hgroup -->
											<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.</p>
											<span class="button">Read More</span>
										</div><!-- .content -->
									</a><!-- .block -->

									
								</div><!-- .item -->
							</div><!-- .col -->
							
						</div><!-- .grid.eqh -->
					</div><!-- .article-body -->
				
				</div><!-- .sw -->
			</div><!-- .filter-contents -->
		</section><!-- .filter-section -->
		
	</div><!-- .body -->
			

<?php include('inc/i-footer.php'); ?>