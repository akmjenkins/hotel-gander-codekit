<?php $bodyclass = 'error404'; ?>
<?php include('inc/i-header.php'); ?>

	<div class="hero single">
		<div class="swiper-wrapper">
			<div class="swipe" data-controls="true" data-auto="7">
				<div class="swipe-wrap">

					<div data-src="../assets/images/temp/hero/hero-inside-2.jpg">
						<div class="item">&nbsp;</div>
						
						<div class="caption">
							<div class="sw">
								<h1 class="title">Attractions</h1>
								<span class="sub">Aliquam Risus Eros.</span>

							</div><!-- .sw -->
						</div><!-- .caption -->
					</div>
					
				</div><!-- .swipe-wrap -->
			</div><!-- .swipe -->
		</div><!-- .swiper-wrapper -->
	</div><!-- .hero -->			

	<div class="body">
	
		<div class="breadcrumbs">
			<div class="sw">
				<a href="#" class="fa fa-abs fa-home">Home</a>
					<a href="#">Attractions</a>
			</div><!-- .sw -->
		</div><!-- .breadcrumbs -->
			
		<section class="sw cf">
			<div class="main-body">
				<div class="article-body">
					
						<p class="excerpt">
							Donec at augue nec ante hendrerit venenatis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Donec adipiscing ut sem tempus sodales. 
						</p>
						
						<p>
							Nullam malesuada leo in risus dictum ullamcorper. Fusce elementum, lorem vel varius aliquam, justo massa dignissim tortor, in tempor eros arcu nec ipsum. In tempus mattis libero, 
							sit amet placerat nisl ultrices in. Nulla a fermentum sem. Proin in diam ut enim tristique lobortis. Phasellus porta mollis erat, quis porttitor purus vehicula eu. Vestibulum sit amet 
							lectus magna. Nam et rhoncus turpis. Sed nec feugiat ligula. Donec at erat eros. Quisque eu convallis dui.
						</p>
					
				</div><!-- .article-body -->
			</div><!-- .main-body -->			
			
		</section><!-- .sw -->
		
		<section class="nopad">
		
			<div class="tab-wrapper">
				<div class="tab-controls attraction-blocks">
					<div class="sw">
					
						<div class="selector fa fa-angle-down fa-abs mobile-selector">
							<select class="tab-controller">
								<option selected>Dining</option>
								<option>Shopping</option>
								<option>Attractions</option>
							</select>
							<span class="value">&nbsp;</span>
						</div><!-- .selector -->

						<div class="grid eqh nopad blocks collapse-750">
						
							<div class="col-3 col">
								<div class="item">
								
									<a class="block with-img attraction-block tab-control selected">
										<div class="img-wrap">
											<span class="bubble fa fa-abs fa-spoon">Dining</span>
										</div><!-- .img-wrap -->
										<div class="content">							
											<div class="hgroup">
												<h3 class="title">Dining</h3>
												<span class="subtitle h6-style">Etiam enim lorem, aliquam a iaculis</span>
											</div><!-- .hgroup -->
											<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.</p>
											<span class="button">View Dining</span>
										</div><!-- .content -->
									</a><!-- .block -->
									
								</div><!-- .item -->
							</div><!-- .col -->

							<div class="col-3 col">
								<div class="item">
								
									<a class="block with-img attraction-block tab-control">
										<div class="img-wrap">
											<span class="bubble fa fa-abs fa-tag">Shopping</span>
										</div><!-- .img-wrap -->
										<div class="content">
											<div class="hgroup">
												<h3 class="title">Shopping</h3>
												<span class="subtitle h6-style">Etiam enim lorem, aliquam a iaculis</span>
											</div><!-- .hgroup -->
											<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.</p>
											<span class="button">View Shopping</span>
										</div><!-- .content -->
									</a><!-- .block -->
									
								</div><!-- .item -->
							</div><!-- .col -->

							<div class="col-3 col">
								<div class="item">
								
									<a class="block with-img attraction-block tab-control">
										<div class="img-wrap">
											<span class="bubble fa fa-abs fa-flag">Attractions</span>
										</div><!-- .img-wrap -->
										<div class="content">
											<div class="hgroup">
												<h3 class="title">Attractions</h3>
												<span class="subtitle h6-style">Etiam enim lorem, aliquam a iaculis</span>
											</div><!-- .hgroup -->
											<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.</p>
											<span class="button">View Attractions</span>
										</div><!-- .content -->
									</a><!-- .block -->
									
								</div><!-- .item -->
							</div><!-- .col -->
							
						</div><!-- .grid.eqh -->
					</div><!-- .sw -->
				</div><!-- .tab-controls -->
				
				<div class="tab-holder directions-tabs">
					<div class="tab selected">
						<div class="embedded-gmap">
							<iframe
								frameborder="0" style="border:0"
								src="https://www.google.com/maps/embed/v1/search?key=<?php echo $GOOGLE_MAPS_API_KEY; ?>&zoom=13&center=48.949243,-54.615094&q=restaurants">
							</iframe>
						</div><!-- .embedded-gmap -->
					</div><!-- .tab -->
					<div class="tab">
					
						<div class="embedded-gmap">
							<iframe
								frameborder="0" style="border:0"
								src="https://www.google.com/maps/embed/v1/search?key=<?php echo $GOOGLE_MAPS_API_KEY; ?>&zoom=13&center=48.949243,-54.615094&q=shopping">
							</iframe>
						</div><!-- .embedded-gmap -->
						
					</div><!-- .tab -->
					<div class="tab">
						<div class="sw">
							<div class="grid">
								<div class="col-2 col sm-col-1">
								
									<h3>What are you looking for?</h3>
									
									<p>Type your keywords or phrase into our search box to quickly find what you are looking for.</p>
								</div><!-- .col -->
								<div class="col-2 col sm-col-1">
									<form action="/" class="single-form" id="activities-form">
										<fieldset>
											<input type="text" placeholder="Enter search terms...">
											<button type="submit" class="fa fa-abs fa-search">Search</button>
										</fieldset>
									</form>
								</div><!-- .col -->
							</div><!-- .grid -->
						</div><!-- .sw -->
					
						<div id="activities-map" class="embedded-gmap" data-zoom="13" data-center="48.949243,-54.615094">
							
						</div><!-- #activites-map -->
					</div>
				</div><!-- .tab-holder -->

			</div><!-- .tab-wrapper -->
		
		</section><!-- .no-pad -->

	
	</div><!-- .body -->
			

<?php include('inc/i-footer.php'); ?>