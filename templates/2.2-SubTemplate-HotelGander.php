<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

	<div class="hero single">
		<div class="swiper-wrapper">
			<div class="swipe" data-controls="true" data-auto="7">
				<div class="swipe-wrap">

					<div data-src="../assets/images/temp/hero/hero-inside-2.jpg">
						<div class="item">&nbsp;</div>
						
						<div class="caption">
							<div class="sw">
								<h1 class="title">About Us</h1>
								<span class="sub">Aliquam Risus Eros.</span>

							</div><!-- .sw -->
						</div><!-- .caption -->
					</div>
					
				</div><!-- .swipe-wrap -->
			</div><!-- .swipe -->
		</div><!-- .swiper-wrapper -->
	</div><!-- .hero -->			

	<div class="body">
	
		<div class="breadcrumbs">
			<div class="sw">
				<a href="#" class="fa fa-abs fa-home">Home</a>
					<a href="#">News</a>
					<a href="#">Morbi Malesuada Nibh</a>
			</div><!-- .sw -->
		</div><!-- .breadcrumbs -->
	
		<article>
			
			<section class="sw cf">
				<div class="main-body with-sidebar">
					<div class="article-body">
						
						<p class="excerpt">
							Donec at augue nec ante hendrerit venenatis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Donec adipiscing ut sem tempus sodales. 
						</p>
						
						<p>
							Nullam malesuada leo in risus dictum ullamcorper. Fusce elementum, lorem vel varius aliquam, justo massa dignissim tortor, in tempor eros arcu nec ipsum. In tempus mattis libero, 
							sit amet placerat nisl ultrices in. Nulla a fermentum sem. Proin in diam ut enim tristique lobortis. Phasellus porta mollis erat, quis porttitor purus vehicula eu. Vestibulum sit amet 
							lectus magna. Nam et rhoncus turpis. Sed nec feugiat ligula. Donec at erat eros. Quisque eu convallis dui.
						</p>
						
						<h5>Donec at Augue nec Ante</h5>
						
						<p>
							Duis sagittis luctus risus sed ornare. Maecenas sollicitudin purus non tempor facilisis. Mauris a dictum lectus, ac laoreet est. Nulla facilisi. Duis imperdiet, mi at hendrerit hendrerit, 
							tortor lectus pretium odio, mollis pulvinar nisi nisi accumsan nisi. Donec ut aliquet turpis. Cras nec nisi consequat, lacinia purus eget, ullamcorper orci. Vivamus mauris turpis, 
							iaculis nec imperdiet ac, varius ut nunc.
						</p>
							
						<p>
							Quisque feugiat mauris mi, ac fringilla erat rutrum non. Morbi consequat massa in massa euismod, ac suscipit sem aliquam. Sed libero felis, feugiat eu hendrerit sit amet, 
							tincidunt gravida purus. Aenean aliquam erat a tincidunt vestibulum. Curabitur placerat lacus at risus ornare convallis. Nulla auctor quis lorem ac adipiscing. Maecenas 
							convallis et velit ac posuere. Duis dictum felis sit amet dui sollicitudin, eget posuere sapien viverra. Cras porttitor ornare odio, eu faucibus lorem convallis eu.
						</p>
						
						<h5>Donec at Augue nec Ante</h5>
						
						<p>
							Etiam ac cursus nisl. In nec hendrerit felis. Cras id sem at ante euismod semper. Sed placerat nisi tellus. Integer enim ipsum, suscipit non aliquam a, pulvinar eget turpis. 
							Suspendisse potenti. Maecenas laoreet eget nulla in pharetra. Cras eget fermentum dui. Mauris volutpat ultrices erat sit amet aliquam. Nunc dolor mauris, mattis at 
							dignissim at, malesuada eget turpis. 
						</p>

						<p>
							Mauris dapibus egestas dui, in fringilla arcu condimentum posuere. Sed luctus diam sit amet erat iaculis sollicitudin. Etiam id varius eros. Nunc ornare scelerisque nisl, 
							quis dapibus ligula facilisis ut. Quisque vel urna sit amet nunc lacinia iaculis.
						</p>
						
					</div><!-- .article-body -->
				</div><!-- .main-body -->
				<aside class="sidebar">
					
					<div class="side-nav">
						<a href="#" class="full button dark-fill reverse selected">Link One</a>
						<a href="#" class="full button dark-fill reverse">Link Two</a>
						<a href="#" class="full button dark-fill reverse">Link Three</a>
					</div>
					
					<a href="#" class="callout-wrap">
						<div class="callout fixedh fixedh-625" style="background-image: url(../assets/images/temp/callout.jpg);">
							<div class="content">
								<span class="title">A Home Away From Home</span>
						
								<span class="button dark-bg full">More Info</span>
							</div><!-- .content -->
						</div><!-- .callout -->
					</a><!-- .callout-wrap -->
					
				</aside><!-- .sidebar -->
			</section><!-- .sw -->
		
		</article>
	
	</div><!-- .body -->
			

<?php include('inc/i-footer.php'); ?>