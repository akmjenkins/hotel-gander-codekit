<?php $bodyclass = 'error404'; ?>
<?php include('inc/i-header.php'); ?>

	<div class="hero single">
		<div class="swiper-wrapper">
			<div class="swipe" data-controls="true" data-auto="7">
				<div class="swipe-wrap">

					<div data-src="../assets/images/temp/hero/hero-inside-1.jpg">
						<div class="item">&nbsp;</div>
						
						<div class="caption">
							<div class="sw">
								<h1 class="title">Attractions</h1>
								<span class="sub">Aliquam Risus Eros.</span>

							</div><!-- .sw -->
						</div><!-- .caption -->
					</div>
					
				</div><!-- .swipe-wrap -->
			</div><!-- .swipe -->
		</div><!-- .swiper-wrapper -->
	</div><!-- .hero -->			

	<div class="body">
	
		<div class="breadcrumbs">
			<div class="sw">
				<a href="#" class="fa fa-abs fa-home">Home</a>
					<a href="#">Attractions</a>
			</div><!-- .sw -->
		</div><!-- .breadcrumbs -->
		
		<section class="light split-screen article-body collapse-800 with-border">
			<div class="sw">
				<div class="grid split-screen-grid collapse-800">
				
					<div class="col-2 col">
						<div class="item">
							
							<a class="overview-block with-img" href="#">
								
								<div class="img-wrap">
									<div class="img" style="background-image: url(../assets/images/temp/ov-block-1.jpg);"></div>
								</div><!-- .img-wrap -->
								
								<div class="content article-body">
									<div class="hgroup">
										<h2 class="title">Dining</h2>
										<span class="h6-style subtitle">Fusce sem mi, tempus nec purus vitae</span>
										
										<p>
											Morbi dui augue, feugiat sit amet tristique vel, fermentum quis sem. Cras enim velit, fringilla eu eros et, hendrerit vestibulum ex. 
											Cras sit amet sollicitudin orci, sit amet aliquam augue. In hac habitasse platea dictumst. Vestibulum at lorem elit.
										</p>
										
										<span class="button">More Info</span>
										
									</div><!-- .hgroup -->
								</div><!-- .content -->
								
							</a><!-- .overview-block -->
								
							
						</div><!-- .item -->
					</div><!-- .col -->
					
					<div class="col-2 col">
						<div class="item">
						
							<a class="overview-block with-img" href="#">
								
								<div class="img-wrap">
									<div class="img" style="background-image: url(../assets/images/temp/ov-block-2.jpg);"></div>
								</div><!-- .img-wrap -->
								
								<div class="content article-body">
									<div class="hgroup">
										<h2 class="title">Shopping</h2>
										<span class="h6-style subtitle">Fusce sem mi, tempus nec purus vitae</span>
										
										<p>
											Morbi dui augue, feugiat sit amet tristique vel, fermentum quis sem. Cras enim velit, fringilla eu eros et, hendrerit vestibulum ex. 
											Cras sit amet sollicitudin orci, sit amet aliquam augue. In hac habitasse platea dictumst. Vestibulum at lorem elit.
										</p>
										
										<span class="button">More Info</span>
										
									</div><!-- .hgroup -->
								</div><!-- .content -->
								
							</a><!-- .overview-block -->

							
						</div>
					</div><!-- .col -->
					
				</div><!-- .grid -->
			</div><!-- .sw -->
		</section>
	
		<section>
			<div class="sw">

				<a class="overview-block with-img" href="#">
					
					<div class="img-wrap">
						<div class="img" style="background-image: url(../assets/images/temp/ov-block-3.jpg);"></div>
					</div><!-- .img-wrap -->
					
					<div class="content article-body">
						<div class="hgroup">
							<h2 class="title">Attractions</h2>
							<span class="h6-style subtitle">Fusce sem mi, tempus nec purus vitae</span>
							
							<p>
								Morbi dui augue, feugiat sit amet tristique vel, fermentum quis sem. Cras enim velit, fringilla eu eros et, hendrerit vestibulum ex. 
								Cras sit amet sollicitudin orci, sit amet aliquam augue. In hac habitasse platea dictumst. Vestibulum at lorem elit.
							</p>
							
							<span class="button">More Info</span>
							
						</div><!-- .hgroup -->
					</div><!-- .content -->
					
				</a><!-- .overview-block -->
			
			</div><!-- .sw -->
		</section>
	
	</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>