var ns = 'GANDER';
window[ns] = {};

// @codekit-append "scripts/components/placeholder.js"; 
// @codekit-append "scripts/components/anchors.external.popup.js"; 
// @codekit-append "scripts/components/standard.accordion.js"
// @codekit-append "scripts/components/custom.select.js";
// @codekit-append "scripts/components/magnific.popup.js"; 
// @codekit-append "scripts/components/tabs.js"; 

// Used in date.inputs.js
// @codekit-append "modules/datepicker/datepicker.dev.js" 

// @codekit-append "scripts/components/date.inputs.js"

// Used in swipe.srf.js
// @codekit-append "scripts/components/swipe.js"; 
// @codekit-append "scripts/components/srf.js"; 

// @codekit-append "scripts/swipe.srf.js"; 

// @codekit-append "scripts/search.js"; 
// @codekit-append "scripts/blocks.js"; 
// @codekit-append "scripts/directions.activities.js"; 
// @codekit-append "scripts/reservation.form.js";
// @codekit-append "scripts/hero.js"; 
// @codekit-append "scripts/nav.js";

// Additional Modernizr Tests
// @codekit-append "modernizr_tests/ios.js";
// @codekit-append "modernizr_tests/geolocation.js";
// @codekit-append "modernizr_tests/android.js";


(function(context) {
	
		//IE8 doesn't support SVGs
		if(!Modernizr.svg) {
			$('img').each(function() {
				var src = this.src;
				if(src.indexOf('.svg') !== -1) {
					this.src = src.replace(/\.svg$/,'.png');
				}
			});
		}

		
		//event map
		$('#view-map')
			.on('click',function(e) {
				$('div.event-map').each(function() {
					var
						el = $(this),
						map = el.children('div.map').data('map')
						
					el.toggleClass('visible');
					google.maps.event.trigger(map.map,'resize');
					map.map.setCenter(map.mapOptions.center);
				});
			});

}(window[ns]));