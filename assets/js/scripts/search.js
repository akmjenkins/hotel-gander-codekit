(function(context) {

	var 
		query = '',
		req,
		$html = $('html'),
		$overlay = $('div.search-overlay'),
		$results = $overlay.find('div.search-results'),
		$form = $overlay.find('form'),
		$input = $form.children('input'),
		url = $form.attr('action'), 
		showclass = 'show-search',
		loadingclass = 'is-loading',
		noresultsclass = 'no-results',
		withresultsclass = 'with-results',
		methods = {
			
			scrollResultsToTop: function() {
				$results.find('div.results').scrollTop(0);
			},
	
			finishedSearch: function(r,status,jqXHR) {
				if(status === 'success' && jqXHR && r) {
					$results.html(r);
					$results.find('select').trigger('change');
					$overlay.removeClass(noresultsclass).addClass(withresultsclass);
					return;
				}
				
				this.failedSearch(arguments);
				
			},
			
			failedSearch: function(jqXHR,status,error) {
				if(status !== 'abort') {
					alert('No results found for '+query);
				}
			},
			
			completedRequest: function() {
				this.hideLoading();
				$input.blur();
			},
			
			showLoading: function() {
				$overlay.addClass(loadingclass);
			},
			
			hideLoading: function() {
				$overlay.removeClass(loadingclass);
			},
		
			doSearch: function(q) {
				var self = this,dfd = $.Deferred();
				
				query = q;
				
				self.showLoading();
				
				req && req.abort();				

				//for effect
				setTimeout(function() {

					req = $.ajax({
						url:url+'?q='+q
					})
						.done(function() {
							dfd.resolve();
							self.finishedSearch.apply(self,arguments);
						})
						.fail(function() {
							dfd.reject();
							self.failedSearch.apply(self,arguments);
						})
						.always(function() {
							self.completedRequest.apply(self,arguments);
						});

				},500);

					
				return dfd.promise();
			
			},
		
			showForm: function() {
				this.isVisible() || $overlay.addClass(noresultsclass).removeClass(withresultsclass);
				$html.addClass(showclass);
				$input.val('');
				Modernizr.touch || $input.focus();

			},
			
			hideForm: function() {
				$html.removeClass(showclass);
				$input.blur();
			},
			
			isVisible: function() {
				return $html.hasClass(showclass);
			},
			
			toggleForm: function() {
				if(!this.isVisible()) {
					return this.showForm();
				}
				
				return this.hideForm();
			}
	
		};
	
	$(document)
		.on('click','.toggle-search-form',function(e) {
			e.preventDefault();
			methods.toggleForm();
		})
		.on('keydown',function(e) {
			e.keyCode === 27 && methods.hideForm();
		});
		
	$overlay
		.on('change','select.tab-controller',function(e) {
			methods.scrollResultsToTop();
		});
	
	$form.on('submit',function(e) {
		methods.doSearch($input.val());
		return false;
	});

	return methods;


}(window[ns]));