(function(context) {
	
	/**
		A simple helper to process JavaScript cookies
		
		If no value is supplied, this function is a getter, otherwise, it's a setter
		
		@param 	name	-	string	-	required	-	the name of the cookie
		@param 	val		-	mixed	-	optional		-	the value of the cookie
		@param 	expiry	-	int		-	optional		-	the number of days (from now) to expire the cookie
		@return 	string or bool								-	if a setter, will return true or false if the cookie is set, otherwise, return the value of the cookie (or false if no cookie was found)
		@author 	Adam Jenkins
	*/
	var cookieHelper = function(name,val,expiry) {
		if(name === undefined) { return false; }
		
		//getter
		if(val === undefined) {
			var arr,i;
			if(document.cookie && document.cookie !== '') {
				arr = document.cookie.split('; ');
				for(i=0;i<arr.length;i++) {
					if(arr[i].match(new RegExp(name+'='))) {
						return arr[i].split(/=/)[1];
					}
				}
			}
		//setter
		} else {
			document.cookie = name + '=' + escape(val) + ((expiry === undefined || isNaN(expiry)) ? "" : "; expires="+((new Date((new Date()).setDate((new Date()).getDate()+100))).toUTCString()));
			return true;
		}
	
		//something must've went awry
		return false;
	
	}
	
	//return the API
	$.extend(context,{
		cookieHelper:cookieHelper
	});
	
}(window[ns]));