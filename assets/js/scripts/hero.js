(function(context) {

	var SwipeSRF = context.SwipeSRF;


	$('div.hero').each(function() {
		var
			el = $(this),
			swiperEl = $('div.swipe',el),
			swiper = swiperEl.data('swipe'),
			allSlides = swiperEl.find('div.swipe-wrap').children(),
			realSlides = allSlides.filter(function() { return !$(this).hasClass('ghost'); }),
			fakeSlides = allSlides.filter(function() { return realSlides.index(this) === -1 });
			utils = {
			
				loadBackgroundImageForSlideAtIndex: function(i) {
					
					var 
						slide = realSlides.eq(i),
						source = slide.data('src');
						
					if(slide.hasClass('loaded')) { return; }
					
					$('<img/>')
						.on('load',function() {
							realSlides
								.eq(i)
								.add(fakeSlides.eq(i))
								.children('.item')
								.css('backgroundImage','url('+source+')')
								.parent()
								.addClass('loaded');
							
							//first load
							if(i === 0) {
								el.addClass('loaded');
							}
							
						})
						.attr('src',source);
					
				}
			
			};
			
			swiperEl.on('swipeChanged',function(e,i) {
				utils.loadBackgroundImageForSlideAtIndex(i);
			});
			
			//load the first background image immediately
			utils.loadBackgroundImageForSlideAtIndex(0);
			
	});

}(window[ns]));