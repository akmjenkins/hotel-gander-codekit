(function(context) {

	var 
		$document = $(document),
		$body = $('body'),
		$nav = $('nav'),
		SHOW_CLASS = 'show-nav',
		ANIMATING_CLASS = 'animating animating-nav';


	//mobile nav
	(function() {
	
		var cb = function(e) { 
			var el = e.target ? e.target : e.srcElement;
			
			if($(el).hasClass('page-wrapper')) {
				$body.removeClass(ANIMATING_CLASS);
			}
		}
		$document.on('click','#mobile-nav',function(e) {
			methods.toggleNav();
			return false;
		});
		
		$body
			.on({
				'webkitTransitionEnd': cb,
				'msTransitionEnd': cb,
				'oTransitionEnd': cb,
				'otransitionend': cb,
				'transitionend': cb
			});
		
	}());

	var methods = {

		showNav: function(show) {
			$body[show ? 'addClass' : 'removeClass'](SHOW_CLASS);

			if(Modernizr.csstransforms3d) {
				$body.addClass(ANIMATING_CLASS);
			}

		},

		toggleNav: function() {
			this.showNav(!this.isShowingNav());
		},

		isShowingNav: function() {
			return $body.hasClass(SHOW_CLASS);
		},
		
		selectSublinkAtIndex: function(i,ctx) {
			var
				links = $('ul li',ctx),
				meta = $('div.link-meta',ctx);
				
				links.removeClass('selected').eq(i).addClass('selected');
				
				this.loadImageForDropdownEl(
					meta
						.children('div')
						.removeClass('active')
						.eq(i)
						.addClass('active')
						.find('div.img')
				);
				
		},
		
		loadImageForDropdownEl: function(el) {
			var imgURL;
			if(el.length && !el.hasClass('loaded')) {
			
				imgURL = templateJS.templateURL+'/'+el.data('img');
				$('<img/>')
					.on('load',function() {
						el
							.css({
								'background-image':'url('+imgURL+')'
							})
							.addClass('loaded');
					})
					.attr('src',imgURL);
			}
			
		}

	};
	
	$nav
		.on('mouseenter mouseleave','>ul>li',function() {
			methods.selectSublinkAtIndex(0,$(this).children('div'));
		})
		.on('mouseenter','ul ul li',function() {
			var
				el = $(this);
				ctx = el.closest('div');
				
			methods.selectSublinkAtIndex(el.index(),ctx);
		});

}(window[ns]));